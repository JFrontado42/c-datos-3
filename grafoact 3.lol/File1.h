#ifndef File.h
#define File.h

#include <cstdlib>
#include <string>
#include <ctime>
#include <vcl.h>

struct prela{
	AnsiString nombre;
	prela *sig;
};

	struct posicion{
	int xOrig;
	int yOrig;
};

struct id{
	int id,early,last;
	posicion pos,posid,posear,poslast;
	bool finale;
};

struct Tarea{
	int duracion;
	AnsiString etiq;
	prela *prelacion;
	Tarea *sig;
	id *orig,*dest;
	bool cross;
};

class Grafo{
public:
struct Tarea *start;

public:
Tarea*crearTarea(){//inicializador de tarea
	Tarea*aux;
	aux=new Tarea;
	aux->duracion=0;
	aux->etiq="";
	aux->prelacion=NULL;
	aux->sig=NULL;
	aux->orig=new id;
	aux->orig->id=0;
	aux->orig->early=0;
	aux->orig->last=0;
	aux->dest=new id;
	aux->dest->id=0;
	aux->dest->early=0;
	aux->dest->last=0;
	aux->orig->pos.xOrig=0;
	aux->orig->pos.yOrig=0;
	aux->dest->pos.xOrig=0;
	aux->dest->pos.yOrig=0;
	aux->orig->posid.xOrig=0;
	aux->orig->posid.yOrig=0;
	aux->orig->posear.xOrig=0;
	aux->orig->posear.yOrig=0;
	aux->orig->poslast.xOrig=0;
	aux->orig->poslast.yOrig=0;
	aux->orig->finale=true;
	aux->dest->finale=true;
	return aux;
}

private:
Tarea *buscarTarea(Tarea *ta,AnsiString ABuscar){//Busca una tarea por la etiqueta y es retornada
	if(ta!=NULL){
		if(ta->etiq==ABuscar){
			return ta;
		}else{
			return buscarTarea(ta->sig,ABuscar);
		}
	}else{
		return NULL;
	}
}

private:
int BuscarMayor(Tarea*todo,prela*pre, int &j){
	if(pre!=NULL){
		if(buscarTarea(todo,pre->nombre)->orig->early+buscarTarea(todo,pre->nombre)->duracion>j){//si es mayor, la funcion retorna el valor de la prelacion encontrada
			j=buscarTarea(todo,pre->nombre)->orig->early+buscarTarea(todo,pre->nombre)->duracion;
			return BuscarMayor(todo,pre->sig,j);
		}else{
			return BuscarMayor(todo,pre->sig,j);
		}
	}else{
		return j;
	}
}

private:
Tarea *&buscarTareaModif(Tarea *&ta,AnsiString ABuscar){//busca una tarea para ser devuelta y modificada
	if(ta!=NULL){
		if(ta->etiq==ABuscar){
			return ta;
		}else{
			return buscarTareaModif(ta->sig,ABuscar);
		}
	}else{
		return ta;
	}
}

private:
void recorreprelaEnum(Tarea*&ta,prela*pre,int i){//agrega las prelaciones
	if(pre!=NULL){
		ta->dest->id=i;
		ta->dest->early=BuscarMayor(start,ta->prelacion,0)+ta->duracion;
		ta->orig=buscarTarea(start,ta->prelacion->nombre)->dest;
		recorreprelaEnum(ta,pre->sig,i);
	}
}

public:
void enumerarNodos(Tarea*&ta,int i){//le inserta el id a cada nodo
	if(ta!=NULL){
		if(ta==start){
			ta->orig->id=1;//asigna id al nodo primero u origen
			ta->orig->early=0;//asigna tiempo early, que al principio es 0
			ta->orig->last=0;//asigna el last que tambien debe ser 0
			ta->dest->id=2;//asigna el id 2 al nodo destino
			ta->dest->early=ta->duracion;//el early de la prelacion 1 siempre es la duracion del primer nodo
			enumerarNodos(ta->sig,3);
		}else{//si no es el primer nodo
			if(ta->prelacion==NULL){//inserta sin prelaciones
				ta->dest->id=i;
				ta->dest->early=ta->duracion;
			}else{
				recorreprelaEnum(ta,ta->prelacion,i);
				//se busca el mayor, se le asigna el valor, y yap
			}
			enumerarNodos(ta->sig,i+1);
		}
	}
}

private:
Tarea*buscapreladasPor(AnsiString busc,Tarea*ta,prela*pre){//busca la tarea en la que la cadena de texto se halle entre sus prelaciones
	if(ta!=NULL){
		if(pre!=NULL){
			if(pre->nombre==busc){
				return ta;
			}else{
				return buscapreladasPor(busc,ta,pre->sig);
			}
		}else{
			if(ta->sig!=NULL){
				return buscapreladasPor(busc,ta->sig,ta->sig->prelacion);
			}
		}
	}else{
		return NULL;
	}
}

private:
int contarPreladasPor(AnsiString busc,Tarea*ta,prela*pre,int &i){//busca la tarea en la que la cadena de texto se halle entre sus prelaciones
	if(ta!=NULL){
		if(pre!=NULL){
			if(pre->nombre==busc){
				i++;
				return contarPreladasPor(busc,ta,pre->sig,i);
			}else{
				return contarPreladasPor(busc,ta,pre->sig,i);
			}
		}else{
			if(ta->sig!=NULL){
				return contarPreladasPor(busc,ta->sig,ta->sig->prelacion,i);
			}
		}
	}else{
		return i;
	}
}

private:
Tarea*&totaltareaspreladas(Tarea*&vacia,AnsiString busc,int i,Tarea*tb,prela *pre){
	if(contarPreladasPor(busc,start,start->prelacion,0)!=0){
		if (vacia==NULL){   *vacia;
			vacia=new Tarea[contarPreladasPor(busc,start,start->prelacion,0)];
		}
		if(tb!=NULL){
			if(pre!=NULL){
				if(pre->nombre==busc){
					vacia[i]=*tb;
					return totaltareaspreladas(vacia,busc,i+1,tb,pre->sig);
				}else{
					return totaltareaspreladas(vacia,busc,i,tb,pre->sig);
				}
			}else{
				if(tb->sig!=NULL){
					return totaltareaspreladas(vacia,busc,i,tb->sig,tb->sig->prelacion);
				}
			}
		}else{
			return vacia;
		}
	}else{
		return vacia;
	}
	
}

private:
posicion buscarposicionGrafica(Tarea*ta,posicion pos){//eso busca un nodo al mismo nivel, si existe, suma para que quede debajo de este y asu no se solapen
	if(ta!=NULL){
		if(ta->dest->pos.xOrig==pos.xOrig&&ta->dest->pos.yOrig==pos.yOrig){
			pos.yOrig+=120;
			return buscarposicionGrafica(start,pos);
		}else{
			return buscarposicionGrafica(ta->sig,pos);
		}
	}else{
		return pos;
	}
}

public:
	Grafo(){
		start= NULL;
	}

private:
void aPrela(AnsiString cad,struct prela *&prelaa,int i){//transforma una cadena de prelaciones a una lista enlazada simple de nombres de prelacion
	if(i<cad.Length()){
		if(prelaa==NULL){
			prelaa=new prela;
			prelaa->nombre="";
			prelaa->sig=NULL;
		}
		if(cad.c_str()[i]!=','){
			prelaa->nombre.operator +=(cad.c_str()[i]);
			aPrela(cad,prelaa,i+1);
		}else{
			aPrela(cad,prelaa->sig,i+1);
		}
	}
}

private:
void Grafo::insertaEnNulo(Tarea*&todo,Tarea*inserta){//encolar
	if(todo==NULL){
		todo=new Tarea;
		todo=crearTarea();
		todo=inserta;
	}else{
		insertaEnNulo(todo->sig,inserta);
	}
}

private:
void Grafo::insertaEnPrelaNulo(prela*&todo,prela*inserta){//encolar tarea
	if(todo==NULL){
		todo=new prela;
		*todo=*inserta;
		todo->sig=NULL;
	}else{
		insertaEnPrelaNulo(todo->sig,inserta);
	}
}

private:
bool insertarEnGrafoNulo(Grafo*&g,Tarea*nodoAInsertar){//inicializar grafo
	g=new Grafo;
	g->start=new Tarea;
	g->start=crearTarea();
	g->start=nodoAInsertar;
	return true;
}

private:
void insertarEnPrelacion(AnsiString prelacion,Tarea*&todas,Tarea*&aInsertar){//inserta cuando una tarea tiene prelaciones
	prela *aux;
	aux=NULL;
	aPrela(prelacion,aux,0);
	while(aux!=NULL){
		if(buscarTarea(todas,aux->nombre)!=NULL){//si la tarea a buscar existe se asigna como prelacion
			if(aInsertar->prelacion==NULL){
				aInsertar->prelacion=new prela;
				prela* aux2;
				aux2=new prela;
				*aux2=*aux;
				aux2->sig=NULL;
				aInsertar->prelacion=aux2;
			}else{
				insertaEnPrelaNulo(aInsertar->prelacion,aux);
			}
		}
		aux=aux->sig;
	}
	insertaEnNulo(todas,aInsertar);
}

public:
bool Grafo::insertarTarea(Grafo*&g,AnsiString prela,Tarea*&inserta){//agrega las tareas en el grafo
	if(g==NULL){//la lista esta vacia
		return insertarEnGrafoNulo(g,inserta);
	}else{
		if(buscarTarea(g->start,inserta->etiq)==NULL){
			if(prela==""){//no posee prelaciones
				insertaEnNulo(g->start,inserta);
			}else{
				insertarEnPrelacion(prela,g->start,inserta);
			}
			return true;
		}else{
			return false;//asdf!!!!!!!!!!!!
		}
	}
}

private:
void conectardePrela(Tarea*&ta,prela*pre){
	if(pre!=NULL){
		buscarTareaModif(start,pre->nombre)->dest=ta->orig;
		conectardePrela(ta,pre->sig);
	}
}

public:
void conectarTareas(Tarea*&ta){
	if(ta!=NULL){
		if(ta->prelacion!=NULL){
			conectardePrela(ta,ta->prelacion);
		}else{
			ta->orig=start->orig;//si no posee prelaciones, se le asigna nodo 1
		}
		conectarTareas(ta->sig);
	}
}

public:
void actualizarGrafico(Tarea*a,posicion pos){//actualiza la graficacion de la informacion
	if(a!=NULL){
		if (a==start){
			a->orig->pos.xOrig=120;     //primer nodo
			a->orig->pos.yOrig=0;
			a->orig->posid.xOrig=122;
			a->orig->posid.yOrig=22;
			a->orig->posear.xOrig=152;
			a->orig->posear.yOrig=12;
			a->orig->poslast.xOrig=152;
			a->orig->poslast.yOrig=32;
			a->dest->pos.xOrig=a->orig->pos.xOrig+120;
			a->dest->pos.yOrig=a->orig->pos.yOrig;
			a->dest->posid.xOrig=122+120;
			a->dest->posid.yOrig=22;
			a->dest->posear.xOrig=152+120;
			a->dest->posear.yOrig=12;
			a->dest->poslast.xOrig=152+120;
			a->dest->poslast.yOrig=32;
			/*se revisa si existen mas de una tarea que es prelada por la primera
			tarea, si es asi, se recorre el arreglo devuelto para cambiar las
			posiciones de los mismos*/
			if(totaltareaspreladas(NULL,a->etiq,0,start,start->prelacion)!=NULL){
				int i=0;           *a;
				while(i<contarPreladasPor(a->etiq,start,start->prelacion,0)){
					totaltareaspreladas(NULL,a->etiq,0,start,start->prelacion)[i].cross=true;
					actualizarGrafico(&totaltareaspreladas(NULL,a->etiq,0,start,start->prelacion)[i],pos);
					i++;
				}
			}
			actualizarGrafico(a->sig,a->dest->pos);
		}else{
			if(a->cross==false){
				a->dest->pos.xOrig=pos.xOrig+120;
				a->dest->pos=buscarposicionGrafica(start,a->dest->pos);
				a->dest->posid.xOrig=a->dest->pos.xOrig+2;
				a->dest->posid.yOrig=a->dest->pos.yOrig+22;
				a->dest->posear.xOrig=a->dest->pos.xOrig+32;
				a->dest->posear.yOrig=a->dest->pos.yOrig+22;
				a->dest->poslast.xOrig=a->dest->pos.xOrig+32;
				a->dest->poslast.yOrig=a->dest->pos.yOrig+32;
				if(totaltareaspreladas(NULL,a->etiq,0,start,start->prelacion)!=NULL){
					int i=0;
					while(i<contarPreladasPor(a->etiq,start,start->prelacion,0)){
						totaltareaspreladas(NULL,a->etiq,0,start,start->prelacion)[i].cross=true;
						actualizarGrafico(&totaltareaspreladas(NULL,a->etiq,0,start,start->prelacion)[i],pos);
						i++;
					}
				}
			}
			if(a->sig!=NULL){
				actualizarGrafico(a->sig,a->dest->pos);
			}
		}
	}
}

public:
void tiempoLast(Tarea *ta){
	if(ta->etiq!="u"){
		tiempoLast(ta->sig);
	}else{
		ta->dest->last=ta->dest->early;
	}
	if(ta->orig->last==0){
		ta->orig->last=ta->dest->early-ta->duracion;
	}else{
		if(ta->orig->last<=ta->dest->early-ta->duracion){
			ta->orig->last=ta->dest->early-ta->duracion;
		}
	}
}

void nodosFinales(Tarea*ta){
	if(ta!=NULL){
		if(buscapreladasPor(ta->etiq,start,start->prelacion)!=NULL){
			ta->orig->finale=false;
			ta->dest->finale=false;
		}
		nodosFinales(ta->sig);
	}
}
/*
void crearultimo(Tarea*ta,id*ultimo){
*/
};

#endif

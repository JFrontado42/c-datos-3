object Form1: TForm1
  Left = 126
  Top = 103
  BorderStyle = bsDialog
  Caption = 'Form1'
  ClientHeight = 497
  ClientWidth = 700
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 115
    Height = 241
    Caption = 'Agregar Tarea'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Impact'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 9
      Top = 164
      Width = 97
      Height = 28
      Hint = 'Para agregar otra Tarea'
      Caption = 'Agregar Tarea'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Impact'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object Button1: TButton
      Left = 9
      Top = 207
      Width = 97
      Height = 28
      Hint = 'Finalizar Proyecto'
      HelpType = htKeyword
      Caption = 'Aceptar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Impact'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = Button1Click
    end
    object LabeledEdit1: TLabeledEdit
      Left = 13
      Top = 38
      Width = 89
      Height = 25
      EditLabel.Width = 35
      EditLabel.Height = 17
      EditLabel.Caption = 'Tarea:'
      EditLabel.Layout = tlBottom
      LabelPosition = lpAbove
      LabelSpacing = 3
      TabOrder = 2
      OnKeyPress = LabeledEdit1KeyPress
    end
    object LabeledEdit2: TLabeledEdit
      Left = 12
      Top = 80
      Width = 90
      Height = 25
      EditLabel.Width = 57
      EditLabel.Height = 17
      EditLabel.Caption = 'Prelacion:'
      LabelPosition = lpAbove
      LabelSpacing = 3
      TabOrder = 3
      OnKeyPress = LabeledEdit2KeyPress
    end
    object LabeledEdit3: TLabeledEdit
      Left = 12
      Top = 122
      Width = 90
      Height = 25
      EditLabel.Width = 53
      EditLabel.Height = 17
      EditLabel.Caption = 'Duracion:'
      LabelPosition = lpAbove
      LabelSpacing = 3
      TabOrder = 4
      OnKeyPress = LabeledEdit3KeyPress
    end
  end
  object StringGrid1: TStringGrid
    Left = 0
    Top = 241
    Width = 206
    Height = 248
    ColCount = 3
    FixedCols = 0
    RowCount = 2
    TabOrder = 1
  end
end

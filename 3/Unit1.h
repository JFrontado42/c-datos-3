//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cola.h"
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TButton *Cargar_ABB;
        void __fastcall Cargar_ABBClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif

class ABB {
public:
        String valor;
        ABB *Der,*Izq;

        ABB CrearA (){
                ABB *aux;
                aux->valor=0;
                aux->Der=NULL;
                aux->Izq=NULL;
                return *aux;
        }
        bool EsvacioA (ABB *A){
                if(A==NULL){
                        return true;
                }else{
                        return false;
                }
        }
        String Valor(ABB *A){
                if (!EsvacioA(A)){
                        return A->valor;
                }else{
                        return 0;
                }
        }
        void CrearRaiz (ABB *&A, String I){
                ABB *Aux=new ABB();
                Aux->Izq=NULL;
                Aux->Der=NULL;
                Aux->valor=I;
                A=Aux;
        }
        int operacion(ABB *A){
                if(!EsvacioA(A)){
                        int res;

                        if(A->valor=='*'){
                                return (A->Izq->valor*A->Der->valor);
                        }
                        if(A->valor=='/'){
                                return (A->Izq->valor/A->Der->valor);
                        }
                        if(A->valor=='-'){
                                return (A->Izq->valor-A->Der->valor);
                        }
                        if(A->valor=='+'){
                                return StrToInt(A->Izq->valor)+StrToInt(A->Der->valor);
                        }
                }
        }
        void PosOrden(ABB *&A){
                if(!EsvacioA(A)){
                        PosOrden(A->Izq);
                        PosOrden(A->Der);
                        if(!EsvacioA(A->Izq) && !EsvacioA(A->Der)){
                                A->valor=operacion(A);
                                A->Izq=NULL;
                                A->Der=NULL;
                        }
                }
        }
        void Arbol (ABB *&A,ABB *C,ABB *B){
                ABB *Aux=new ABB();
                if(!EsvacioA(A)){
                        Aux->CrearRaiz(Aux,Valor(A));
                        if(!EsvacioA(C)){
                                Aux->Izq=C;
                        }
                        if(!EsvacioA(B)){
                                Aux->Der=B;
                        }
                        A= Aux;
                }
        }
        int obtenerElementos(Cola *&C, String E){
                if (C->Primero(C) == E){
                        C->Desencolar(C);
                        return 1;
                }else{
                        return 0;
                }
        }
        bool es_numero(String V,int cont){
                char *a;
                a=new char [50];
                a=V.c_str();
                if(int(a[cont])>=48 && int(a[cont])<=57 ){
                        cont++;
                        return es_numero(V,cont);
                }else{
                        if(a[cont]=='�'){
                                return true;
                        }else{
                                return false;
                        }
                }
        }
        ABB *obtenerNumero(Cola *&C){
                ABB *x;
                if(obtenerElementos(C,'(')){
                        x= obtenerSuma(C);
                        if(obtenerElementos(C,')')){
                                return x;
                        }else{
                                //error falta un parentesis
                        }
                }else{
                        if (es_numero(C->Primero(C)+"�",0)==true){
                                CrearRaiz(x,C->Primero(C));
                                C->Desencolar(C);
                                Arbol (x, NULL, NULL);
                                return x;
                        }else{
                                return NULL;
                        }
                }
        }
        ABB *obtenerProducto(Cola *&C){
                ABB *a,*b,*aux;
                a = obtenerNumero(C);
                if (obtenerElementos(C, '*')){
                        b = obtenerProducto(C);
                        CrearRaiz(aux,'*');
                        Arbol (aux, a, b);
                        return aux;
                }else{
                        if (obtenerElementos(C, '/')){
                                b = obtenerProducto(C);
                                CrearRaiz(aux,'/');
                                Arbol (aux, a, b);
                                return aux;
                        }else{
                                return a;
                        }
                }
        }
        ABB *obtenerSuma(Cola *&C){
                ABB *a,*b,*aux;
                a = obtenerProducto(C);
                if (obtenerElementos(C, '+')){
                        b = obtenerSuma(C);
                        CrearRaiz(aux,'+');
                        Arbol (aux, a, b);
                        return aux;
                }else{
                        if (obtenerElementos(C, '-')){
                                b = obtenerSuma(C);
                                CrearRaiz(aux,'-');
                                Arbol (aux, a, b);
                                return aux;
                        }else{
                                return a;
                        }
                }
        }
};


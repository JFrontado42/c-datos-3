//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"

//---------------------------------------------------------------------
USEFORM("Unit1.cpp", Form1);
//---------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int){
	Application->Initialize();
	Application->CreateForm(__classid(TForm1), &Form1);
	Application->Run();

	return 0;
}
//---------------------------------------------------------------------

object Form1: TForm1
  Left = 519
  Top = 198
  Width = 292
  Height = 493
  AutoSize = True
  Caption = 'Trabajo De Cola'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 183
    Height = 161
    Caption = 'Add Node'
    TabOrder = 0
    object Label1: TLabel
      Left = 120
      Top = 28
      Width = 28
      Height = 13
      Caption = 'Name'
    end
    object Label2: TLabel
      Left = 120
      Top = 60
      Width = 51
      Height = 13
      Caption = 'ID Number'
    end
    object Label3: TLabel
      Left = 120
      Top = 92
      Width = 19
      Height = 13
      Caption = 'Age'
    end
    object Edit1: TEdit
      Left = 16
      Top = 24
      Width = 97
      Height = 21
      TabOrder = 0
      Text = 'Name'
      OnClick = Edit1Click
    end
    object Edit2: TEdit
      Left = 16
      Top = 56
      Width = 97
      Height = 21
      TabOrder = 1
      Text = 'ID Number'
      OnClick = Edit2Click
    end
    object Edit3: TEdit
      Left = 16
      Top = 88
      Width = 97
      Height = 21
      TabOrder = 2
      Text = 'Age'
      OnClick = Edit3Click
    end
    object Button1: TButton
      Left = 72
      Top = 120
      Width = 70
      Height = 25
      Caption = 'Add'
      TabOrder = 3
      OnClick = Button1Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 161
    Width = 183
    Height = 305
    Caption = 'List Nodes'
    TabOrder = 1
    object Button2: TButton
      Left = 85
      Top = 267
      Width = 75
      Height = 25
      Caption = 'Update'
      TabOrder = 0
      OnClick = Button2Click
    end
    object StringGrid1: TStringGrid
      Left = 16
      Top = 17
      Width = 154
      Height = 244
      ColCount = 3
      DefaultColWidth = 49
      DefaultRowHeight = 18
      FixedCols = 0
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSizing, goRowSelect]
      TabOrder = 1
    end
  end
  object GroupBox3: TGroupBox
    Left = 184
    Top = 0
    Width = 100
    Height = 59
    Caption = 'Delete First Node'
    TabOrder = 2
    object Button3: TButton
      Left = 13
      Top = 19
      Width = 75
      Height = 25
      Caption = 'Delete'
      TabOrder = 0
      OnClick = Button3Click
    end
  end
  object GroupBox4: TGroupBox
    Left = 184
    Top = 58
    Width = 100
    Height = 59
    Caption = 'Order by name'
    TabOrder = 3
    object Button4: TButton
      Left = 13
      Top = 19
      Width = 75
      Height = 25
      Caption = 'Order'
      TabOrder = 0
      OnClick = Button4Click
    end
  end
  object GroupBox5: TGroupBox
    Left = 184
    Top = 118
    Width = 100
    Height = 59
    Caption = 'Order by ID'
    TabOrder = 4
    object Button5: TButton
      Left = 13
      Top = 19
      Width = 75
      Height = 25
      Caption = 'Order'
      TabOrder = 0
      OnClick = Button5Click
    end
  end
  object GroupBox6: TGroupBox
    Left = 184
    Top = 178
    Width = 100
    Height = 59
    Caption = 'Order by Age'
    TabOrder = 5
    object Button6: TButton
      Left = 13
      Top = 19
      Width = 75
      Height = 25
      Caption = 'Order'
      TabOrder = 0
      OnClick = Button6Click
    end
  end
end

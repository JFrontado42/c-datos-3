#ifndef NODOS
#define NODOS
#include <cstdlib>
#include <vcl.h>

using namespace std;
namespace Persona{
class Per_s {
public:
	Per_s *sig;
	int ci, edad;
	AnsiString nombre;
	//---------------------------------------------------------
	Per_s(){//constructor del objeto de la clase
		ci=0;
		edad=0;
		nombre="";
		sig=NULL;
	}
	//---------------------------------------------------------
	int mayorNom(AnsiString a,AnsiString b){
		for (int i = 0; i < a.Length(); i++) {
			if (a.c_str()[i] > b.c_str()[i]) {
				return 1;
			}else if (a.c_str()[i]<b.c_str()[i]) {
				return 2;
			}
		}
		return 0;
	}
	//---------------------------------------------------------
	bool esvacio(Per_s *a) {//metodo que devuelve booleano
				// si el objeto tiene elementos
				//o no
		if (a==NULL) {
			return true;
		} else {
			return false;
		}
	}
	//---------------------------------------------------------
	//metodo que agrega la info a nodo nuevo
	void Nvo(Per_s *aux,Per_s *&a){
		if (!esvacio(a)){
			Nvo(aux,a->sig);
		}else{
			a=new Per_s;
			*a=*aux;
		}
	}
	//---------------------------------------------------------
	AnsiString primeronom(Per_s *a) {
		if (!esvacio(a)) {
			return a->nombre;
		} else {
			return "";
		}
	}
	//---------------------------------------------------------
	void desencolar(Per_s *&a) {
		Per_s *aux;
		aux=new Per_s;
		if (!esvacio(a)) {
			*aux=*a;
			if (!esvacio(a->sig)){
				*a = *a->sig;
			}else{
				a=NULL;
			}
		}
		delete aux;
	}
	//---------------------------------------------------------
	void encolar(Per_s *&salid, Per_s *entr) {
		if (!esvacio(salid)) {
			encolar(salid->sig,entr);
		} else {
			salid=new Per_s;
			*salid=*entr;
		}
	}
	//---------------------------------------------------------
	int primeroCI(Per_s *a) {
		if (!esvacio(a)) {
			return a->ci;
		} else {
			return 0;
		}
	}
	//---------------------------------------------------------
	int primeroEdad(Per_s *a) {
		if (!esvacio(a)) {
			return a->edad;
		} else {
			return 0;
		}
	}
	//---------------------------------------------------------
	void copiar(Per_s *&salid, Per_s *b) {
		if (!esvacio(b)) {
			encolar(salid,valor(b));
		}
	}
	//---------------------------------------------------------
	Per_s* valor(Per_s *n){
		Per_s *aux=new Per_s;
		*aux=*n;
		aux->sig=NULL;
		return aux;
	}
	//---------------------------------------------------------
	void pasar(Per_s *&salid, Per_s *a) {
		if (!esvacio(salid)) {
			copiar(a, salid);
			desencolar(salid);
			pasar(salid, a);
		}
	}
	//---------------------------------------------------------
	void mayor(Per_s *&a) {
		Per_s *may, *may1;
		may = NULL;
		may1 = NULL;
		buscarmayorCI(a, may, may1);
		*a = *may;
	}
	//---------------------------------------------------------
	void buscarmayorCI(Per_s *a, Per_s *&aux1, Per_s *aux2) {
	*aux1;
//
		if (!esvacio(a)) {
			if (esvacio(aux1)) {
				copiar(aux1, a);
			} else {
				if (primeroCI(a)>primeroCI(aux1)) {
					copiar(aux2, a);
					pasar(aux1, aux2);
					if(esvacio(aux1)){
						aux1=new Per_s;
					}
					*aux1 = *aux2;
					aux2 = NULL;
				} else {
					copiar(aux2, aux1);
					desencolar(aux1);
					if (esvacio(aux1)) {
						copiar(aux2, a);
						if(esvacio(aux1)){
							aux1=new Per_s;
						}
						*aux1 = *aux2;
						aux2 = NULL;
					} else {
						pasarMay(a, aux1, aux2);
					}
				}
			}
			desencolar(a);
			buscarmayorCI(a, aux1, NULL);
		}
//
	}
	//---------------------------------------------------------
	void menor(Per_s *&a) {
		Per_s *men, *men1;
		men = NULL;
		men1 = NULL;
		buscarmenorEda(a, men, men1);
		*a = *men;
		delete men;
		delete men1;
	}
	//---------------------------------------------------------
	void buscarmenorEda(Per_s *a, Per_s *&aux1, Per_s *aux2) {
		if (!esvacio(a)) {
			if (esvacio(aux1)) {
				copiar(aux1, a);
			} else {
				if (primeroEdad(a)<primeroEdad(aux1)) {
					copiar(aux2, a);
					pasar(aux1, aux2);
					if(esvacio(aux1)){
						aux1=new Per_s;
					}
					*aux1 = *aux2;
					aux2 = NULL;
				} else {
					copiar(aux2, aux1);
					desencolar(aux1);
					if (esvacio(aux1)) {
						copiar(aux2, a);
						if(esvacio(aux1)){
							aux1=new Per_s;
						}
						*aux1 = *aux2;
						aux2 = NULL;
					} else {
						pasarMen(a, aux1, aux2);
					}
				}
			}
			desencolar(a);
			buscarmenorEda(a, aux1, NULL);
		}
	}
	//---------------------------------------------------------
	void ordenarNombre(Per_s *a, Per_s *&aux1, Per_s *aux2) {
		if (!esvacio(a)) {
			if (esvacio(aux1)) {
				copiar(aux1, a);
			} else {
				if (primeronom(a)<primeronom(aux1)) {
					copiar(aux2, a);
					pasar(aux1, aux2);
					if(esvacio(aux1)){
						aux1=new Per_s;
					}
					*aux1 = *aux2;
					aux2 = NULL;
				} else {
					copiar(aux2, aux1);
					desencolar(aux1);
					if (esvacio(aux1)) {
						copiar(aux2, a);
						if(esvacio(aux1)){
							aux1=new Per_s;
						}
						*aux1 = *aux2;
						aux2 = NULL;
					} else {
						pasarMen(a, aux1, aux2);
					}
				}
			}
			desencolar(a);
			ordenarNombre(a, aux1, NULL);
		}
	}
	//---------------------------------------------------------
	void ordenar(Per_s *&a) {
	*a;
		Per_s *aux;
		aux=NULL;
		ordenarNombre(a, aux, NULL);
		*a = *aux;
		delete aux;
	}
	//---------------------------------------------------------
	void pasarMen(Per_s *a, Per_s *&b, Per_s *c) {
	*b;
		if (!esvacio(b)) {
			if(primeronom(b) <primeronom(a)){
				copiar(c,b);
				desencolar(b);
			}else{
				copiar(c,a);
				pasar(b,c);
				desencolar(a);
			}
			pasarMen(a,b,c);
		}else{
			b=new Per_s;
			copiar(c,a);
			*b=*c;
			c=NULL;
		}
	}
	//---------------------------------------------------------
	void pasarMay(Per_s *a, Per_s *&b, Per_s *c) {
	*b;
		if (!esvacio(b)) {
			if(primeronom(b) >primeronom(a)){
				copiar(c,b);
				desencolar(b);
			}else{
				copiar(c,a);
				pasar(b,c);
				desencolar(a);
			}
			pasarMay(a,b,c);
		}else{
			b=new Per_s;
			copiar(c,a);
			*b=*c;
			c=NULL;
		}
	}
	//---------------------------------------------------------
	//---------------------------------------------------------
	//--------------------OTRAS FUNCIONES----------------------
	//---------------------------------------------------------
	//---------------------------------------------------------
	int counttotalnodos(Per_s *a,int b){
		if (!esvacio(a)){
			return counttotalnodos(a->sig,b+1);
		}else{
			return b;
		}
	}
};
}
#endif // NODES_H_INCLUDED

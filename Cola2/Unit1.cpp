//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
using namespace Persona;
TForm1 *Form1;
Per_s *obj;  

//--------------------MIS METODOS--------------------------------------------
void recurs(Per_s *a,int i,TStringGrid* StringGrid1){
	if(!a->esvacio(a)){
		StringGrid1->Cells[0][i]=a->nombre;
		StringGrid1->Cells[1][i]=a->ci;
		StringGrid1->Cells[2][i]=a->edad;
		recurs(a->sig,i+1,StringGrid1);
	}
}
void updatList(TStringGrid *StringGrid1){
	int tama=obj->counttotalnodos(obj,0)+1;
	StringGrid1->RowCount=tama;
	StringGrid1->Cells[0][0]="Nombre";
	StringGrid1->Cells[1][0]="Cedula";
	StringGrid1->Cells[2][0]="Edad";
	recurs(obj,1,StringGrid1);
}
void msgVacio(){
	Application->MessageBoxA("The list is empty","Empty",0);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void msgModif(const char *txt1,const char *txt2){
	Application->MessageBoxA(txt1,txt2,0);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Edit1Click(TObject *Sender)
{
	if(!Edit1->Modified){
		Edit1->Clear();
	}else{
		Edit1->SelectAll();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit2Click(TObject *Sender)
{
	if(!Edit2->Modified){
		Edit2->Clear();
	}else{
		Edit2->SelectAll();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit3Click(TObject *Sender)
{
	if(!Edit3->Modified){
		Edit3->Clear();
	}else{
		Edit3->SelectAll();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
	if(!Edit1->Modified||!Edit2->Modified||!Edit3->Modified){
		msgModif("You have not modified the text in the content, please change it","Change Content");
	}else{  //esto es si se han modificado los cuadritos
		Per_s *aux;                    //se crea el auxiliar
		aux=new Per_s;                 //se inicializa
		aux->nombre=Edit1->Text;       //a el texto en el cuadro edit1 se le asigna el nombre de la variable aux
		Edit1->Clear();
		aux->ci=Edit2->Text.ToInt();
		Edit2->Clear();
		aux->edad=Edit3->Text.ToInt();
		Edit3->Clear();
		aux->Nvo(aux,obj);
		delete aux;
		updatList(StringGrid1);
	}                                      //todo esto hace el boton ok!
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
	if(obj->esvacio(obj)){
		msgVacio();
	}else{
		updatList(StringGrid1);
	}
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
	if(obj->esvacio(obj)){
		msgVacio();
	}else{
		obj->desencolar(obj);
		updatList(StringGrid1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
	if(obj->esvacio(obj)){
		msgVacio();
	}else{
		obj->ordenar(obj);
		updatList(StringGrid1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
	if(obj->esvacio(obj)){
		msgVacio();
	}else{
		obj->mayor(obj);
		updatList(StringGrid1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button6Click(TObject *Sender)
{
	if(obj->esvacio(obj)){
		msgVacio();
	}else{
		obj->menor(obj);
		updatList(StringGrid1);
	}
}
//---------------------------------------------------------------------------


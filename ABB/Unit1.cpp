//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop


#include "Unit1.h"
#include "ArbolABB.h"
#include "Pila.h"
#include "Cola.h"
#include <cstdlib>
#include <iostream>
#include <vcl.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
Pila *pila;
ABB *Abb;


//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{       
}
//---------------------------------------------------------------------------
int NodosPI(Pila *a,int b){
	if(a!=NULL){
		NodosPI(a->Anterior,b+1);
	}else{
		return b+1;
	}
}

int NodosCo(Cola *a,int b){
	if(a!=NULL){
		NodosCo(a->Siguiente,b+1);
	}else{
		return b+1;
	}
}

void Mostrar2(Cola *k,int i,TStringGrid* b){
	if(!k->EsVacio(k))
        {
		b->Cells[0][i]=k->Numero;
		Mostrar2(k->Siguiente,i+1,b);
	}
 }
void updatListC(Cola *k,TStringGrid *b){
b->RowCount=NodosCo(k,0);
b->Cells[0][0]="Valor";
Mostrar2(k,1,b);
}


 void Mostrar(Pila *k,int i,TStringGrid* b){
	if(!k->EsVacio(k))
        {
		b->Cells[0][i]=k->Numero;
		Mostrar(k->Anterior,i+1,b);
	}
 }

void updatList(Pila *k,TStringGrid *b){
b->RowCount=NodosPI(k,0);
b->Cells[0][0]="Valor";
Mostrar(k,1,b);
}

void __fastcall TForm1::Button4Click(TObject *Sender)
{
	 Close();	
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Edit1Click(TObject *Sender)
{
	if(!Edit1->Modified){
		Edit1->Clear();
	}else{
		Edit1->SelectAll();
	}	
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
      if(!Edit1->Modified){
     Application->MessageBoxA("Introduzca todos los datos","Faltan Datos",0);
        }else{
            Pila *aux;
		aux=new Pila;
                aux->Anterior=NULL;
		aux->Numero=Edit1->Text.ToInt();
                Edit1->Clear();
                aux->Apilar(pila,aux);
                updatList(pila,StringGrid1);
                }	
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
		Cola *e;
		e=NULL;
		Abb->PreOrden(Abb,e);
                updatListC(e,StringGrid1);
		
		
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
	  	Cola *e;
		e=NULL;
		Abb->InOrden(Abb,e);
                updatListC(e,StringGrid1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button6Click(TObject *Sender)
{
      		Cola *e;
		e=NULL;
		Abb->PostOrden(Abb,e);
                updatListC(e,StringGrid1);	
}
//---------------------------------------------------------------------------
void Insertar(ABB *&a,Pila *&p){
			if(!p->EsVacio(p)){
                               a->InsertarH(a,p->Tope(p));
                               p->desapilar(p);
                               Insertar(a,p);
        	}
}
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	     Insertar(Abb,pila);	
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

void __fastcall TForm1::Edit2Click(TObject *Sender)
{
	    if(!Edit2->Modified){
		Edit2->Clear();
	}else{
		Edit2->SelectAll();
	}
}
//---------------------------------------------------------------------------
  void __fastcall TForm1::Button7Click(TObject *Sender)
{       
       if(!Edit2->Modified){
     Application->MessageBoxA("Introduzca todos los datos","Faltan Datos",0);
        }else{
	Cola *aux,*e;
        e=NULL;
         aux=new Cola;
         aux->Numero=Edit2->Text.ToInt();
         Edit1->Clear();
	 Abb->EliminarH(Abb,aux->Numero);
         Abb->PreOrden(Abb,e); 
         updatListC(e,StringGrid1);
}        }
void __fastcall TForm1::Edit3Click(TObject *Sender)
{
	    if(!Edit3->Modified){
		Edit3->Clear();
	}else{
		Edit3->SelectAll();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button8Click(TObject *Sender)
{
	     if(!Edit3->Modified){
     Application->MessageBoxA("Introduzca todos los datos","Faltan Datos",0);
        }else{
        	if(Abb->BuscarAbb(Abb,Edit3->Text.ToInt())){
                           Application->MessageBoxA("Se encuentra en el arbol","Encontrado",0);
                }else{
                	ShowMessage("El Elemento No se encuentra");
                }
        }
}
//---------------------------------------------------------------------------


object Form1: TForm1
  Left = 189
  Top = 114
  Width = 281
  Height = 444
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 128
    Top = 8
    Width = 81
    Height = 25
    Caption = 'Apilar'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 112
    Top = 128
    Width = 161
    Height = 57
    Caption = 'Cargar ArbolBB'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button4: TButton
    Left = 8
    Top = 368
    Width = 97
    Height = 41
    Caption = 'Salir.'
    TabOrder = 3
    OnClick = Button4Click
  end
  object Edit1: TEdit
    Left = 8
    Top = 8
    Width = 105
    Height = 21
    TabOrder = 6
    Text = 'Inserte Numero.'
    OnClick = Edit1Click
  end
  object Panel1: TPanel
    Left = 112
    Top = 184
    Width = 161
    Height = 225
    Caption = 'Panel1'
    TabOrder = 7
    object Label1: TLabel
      Left = 56
      Top = 1
      Width = 46
      Height = 13
      Caption = 'Recorrido'
    end
  end
  object Button3: TButton
    Left = 120
    Top = 208
    Width = 145
    Height = 65
    Caption = 'Preorden'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button5: TButton
    Left = 120
    Top = 272
    Width = 145
    Height = 65
    Caption = 'Inorden'
    TabOrder = 4
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 120
    Top = 336
    Width = 145
    Height = 57
    Caption = 'Postorden'
    TabOrder = 5
    OnClick = Button6Click
  end
  object StringGrid1: TStringGrid
    Left = 16
    Top = 128
    Width = 73
    Height = 241
    ColCount = 1
    FixedCols = 0
    ScrollBars = ssNone
    TabOrder = 8
  end
  object Button7: TButton
    Left = 128
    Top = 48
    Width = 81
    Height = 33
    Caption = 'Eliminar'
    TabOrder = 9
    OnClick = Button7Click
  end
  object Edit2: TEdit
    Left = 8
    Top = 56
    Width = 105
    Height = 21
    TabOrder = 10
    Text = 'Elimiar'
    OnClick = Edit2Click
  end
  object Edit3: TEdit
    Left = 8
    Top = 88
    Width = 105
    Height = 21
    TabOrder = 11
    Text = 'Buscar'
    OnClick = Edit3Click
  end
  object Button8: TButton
    Left = 128
    Top = 88
    Width = 81
    Height = 25
    Caption = 'Buscar'
    TabOrder = 12
    OnClick = Button8Click
  end
end

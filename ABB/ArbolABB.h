#ifndef ArbolABBH
#define ArbolABBH
#include "Pila.h"
#include "Cola.h"
#include <cstdlib>
#include <iostream>
#include <vcl.h>


class ABB{
        public:
        int Numero;
        ABB *Izq,*Der;

        ABB(){
                Numero=NULL;
                Izq=NULL;
                Der=NULL;
        }
bool EsVacioA(ABB *a){
	if(a==NULL){
                    return true;
        }else{
        return false;
        }
}

int Valor(ABB *a){
                  if(!EsVacioA(a)){
                  	return a->Numero;
                  }else{
                  return NULL;
                  }
}

ABB* CrearRaiz(int v){
ABB *aux;
aux=new ABB;
aux->Numero=v;
return aux;
}

void InsertarH(ABB *&a,int &v){
                              ABB*Aux;
                              Aux=new ABB;
                              Aux=a;
	if(EsVacioA(a)){
        a=CrearRaiz(v);
        }else{
        if(Valor(a)>v){
           InsertarH(Aux->Izq,v);
        }else{
        if(Valor(a)<v){
        InsertarH(Aux->Der,v);
        }else{
        	ShowMessage("El valor esta repetido");
        }
        }
        }
}

void EliminarH(ABB *&a,int v){
	ABB *aux,*aux2,*otro;
        bool b;
        if(!EsVacioA(a)){
        	if(Valor(a)>v){
                	EliminarH(a->Izq,v);
                }else{
                	if(Valor(a)<v){
                        	EliminarH(a->Der,v);
                        	}else{
                                      otro=a;
                                      if(EsVacioA(otro->Der)){
                                      		a=otro->Izq;
                                      }else{
                                      	if(EsVacioA(otro->Izq)){
                                        a=otro->Der;
                                        }else{
                                        	aux=a->Izq;
                                                b=false;
                                                while(!EsVacioA(aux->Der)){
                                                aux2=aux;
                                                aux=aux->Der;
                                                b=true;
                                                }
                                                a->Numero=v;
                                                otro=aux;
                                                if(b){
                                                aux2->Der=aux->Izq;
                                                }else{
                                                	a->Izq=aux->Izq;
                                                }
                                        }
                                      }
                                      delete otro;
                        	}
                }
        }
}

void PreOrden(ABB *a,Cola *&k){
	 if(!EsVacioA(a)){
         	PreOrden(a->Izq,k);
                k->Encolar(k,k->CrearC(Valor(a)));
                PreOrden(a->Der,k);
         }
}
void InOrden(ABB *a,Cola *&k){
	 if(!EsVacioA(a)){
         	k->Encolar(k,k->CrearC(Valor(a)));
                InOrden(a->Izq,k);
                InOrden(a->Der,k);
         }
}
void PostOrden(ABB *a,Cola *&k){
	 if(!EsVacioA(a)){
         	PostOrden(a->Izq,k);
                PostOrden(a->Der,k);
                //ShowMessage(AnsiString(a->Numero));
                k->Encolar(k,k->CrearC(Valor(a)));

         }
}

bool BuscarAbb(ABB *a,int v){
    if(EsVacioA(a)){
    return false;
    }else{
        if(Valor(a)==v){
            return true;
            }else{
                if(Valor(a)>v){
                    return BuscarAbb(a->Izq,v);
                    }else{
                        return BuscarAbb(a->Der,v);
                        }
                }
        }
    }

};
#endif

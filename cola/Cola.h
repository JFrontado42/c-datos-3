#ifndef Cola_cabezera
#define Cola_cabezera
#include <cstdlib>
#include <vcl.h>
using namespace std;
namespace Persona{
class Pers{
    public:
        int ced;
        int edad;
        AnsiString nombre;
        Pers *sig;

    Pers()
      {
        ced=0;
        edad=0;
        nombre="";
        sig=NULL;
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
      bool esvacioc(Pers *l)
      {
           if (l == NULL)
           {
                return true;
           }
           else
           {
               return false;
            }
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
      int primero_ced(Pers *l)
      {
          if (!esvacioc(l))
          {
               return l->ced;
               }
          else
          {
              return 0;
              }
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
      int primero_edad(Pers *l)
      {
        if (!esvacioc(l))
          {
               return l->edad;
               }
          else
          {
              return 0;
              }
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*      
      AnsiString primer_nombre(Pers *l)
      {
          if (!esvacioc(l))
          {
               return l->nombre;
               }
          else
          {
              return "";
              }
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*     
      void desencolar(Pers *&l)
      {
       Pers *aux;
       aux=new Pers;
       	if (!esvacioc(l))
           {
			*aux=*l;
			if (!esvacioc(l->sig))
            {
				*l = *l->sig;
			}
            else
            {
				l=NULL;
			}
		}
      delete aux;
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*      
      void encolar(Pers *&k, Pers *m)
      {
    	if (!esvacioc(k))
         {
                encolar(k->sig,m);
         }
         else
         {
               k=new Pers;
               *k=*m;
         }
	}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*      
      void crear_nodo(Pers *aux, Pers *&l)
      {
         if(!esvacioc(l))
         {
             crear_nodo(aux,l->sig);
             }
         else
         {
             l=new Pers;
             *l=*aux;
         }
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*      
      void copiar(Pers *&a, Pers *b)
      {
          if (!esvacioc(b))
          {
               encolar(a,valor(b));
          }
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
      Pers* valor(Pers *n)
      {
          Pers *aux=new Pers;
          *aux = *n;
          aux->sig=NULL;
          return aux;
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*        
      void pasar(Pers *&a, Pers *b)
      {
          if(!esvacioc(a))
          {
              copiar(b,a);
              desencolar(a);
              pasar(a,b);
          }
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
      void mayor(Pers *&o)
      {
          Pers *may;
          Pers *may2;
          may=NULL;
          may2=NULL;
          Buscar_mayor(o,may,may2);
          if(esvacioc(o)){
          o=new Pers;
          }
          *o=*may;
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
      void Buscar_mayor(Pers *r,Pers *&m,Pers *k)
      { *m;
          if (!esvacioc(r))
          {
              if(esvacioc(m))
              {
                  copiar(m,r);
              }
              else
              {
                  if(primero_ced(r)>primero_ced(m))
                  {
                       copiar(k,r);
                       pasar(m,k);
                       if(esvacioc(m)){
                        m=new Pers;
                        }
                      *m=*k;
                      k=NULL;
                  }
                  else
                  {
                       copiar(m,r);
                  }
              }
              desencolar(r);
              Buscar_mayor(r,m,k);
          }
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
      void menor(Pers *&o)
      {
     
          Pers *men;
          Pers *men2;
          men=NULL;
          men2=NULL;
          Buscar_menor(o,men,men2);
          if(esvacioc(o)){
          o=new Pers;
          }
          *o=*men;
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
      void Buscar_menor(Pers *r,Pers *&m,Pers *k)
      {
          if (!esvacioc(r))
          {
              if(esvacioc(m))
              {
                  copiar(m,r);
              }
              else
              {
                  if(primero_edad(r)<primero_edad(m))
                  {
                       copiar(k,r);
                       pasar(m,k);
                       if(esvacioc(m)) {
                        m=new Pers;
                        }
                       *m=*k;
                       k=NULL;
                  }
                  else
                  {
                       copiar(m,r);
                  }
              }
               desencolar(r);
               Buscar_menor(r,m,k);
          }
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
      void ordenar(Pers *&l)
      {
          Pers *aux;
          Pers *aux1;
          aux=NULL;
          aux1=NULL;                       
          ordenar_nombre(l,aux,aux1);
          if(esvacioc(l)){
                l=new Pers;
          }
          *l=*aux;
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
      void ordenar_nombre(Pers *r,Pers *&m,Pers *k)
      {              *m;
          if(!esvacioc(r))
          {
              if (esvacioc(m))
              {
                   copiar(m,r);
              }
              else
              {
                  if (primer_nombre(r)<primer_nombre(m))
                  {
                      copiar(k,r);
                      pasar(m,k);
                      if(esvacioc(m)){
                        m= new Pers;
                      }
                      *m=*k;
                      k=NULL;
                  }
                  else
                  {
                       copiar(k,m);
                       desencolar(m);
                      if (esvacioc(m))
                      {
                          copiar(k,r);
                          if(esvacioc(m)){
                                m=new Pers;
                          }
                          *m=*k;
                          k=NULL;
                      }
                      else
                      {
                          Pasar_menor(r,m,k);
                      }
                  }
              }
               desencolar(r);
               ordenar_nombre(r,m,k);
          }
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
      void Pasar_menor(Pers *h, Pers *&j, Pers *s)
      {                                 
          if(!esvacioc(j))
          {
              if(primer_nombre(j)<primer_nombre(h))
              {
                   copiar(s,j);
                   desencolar(j);
                   Pasar_menor(h,j,s);
              }
              else
              {
                   copiar(s,h);
                   pasar(j,s);
                   if (esvacioc(j)){
                        j=new Pers;
                   }
                   *j=*s;
                   s=NULL;
              }
          }
          else
          {
             copiar(s,h);
             *j=*s;
             s=NULL;
          }
      }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
          int totalnodos(Pers *a,int b){
		if (!esvacioc(a))
                {
			return totalnodos(a->sig,b+1);
		}
                else
                {
			return b;
		}
	}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   };
}


     








#endif // HEADERCOLA_H_INCLUDED

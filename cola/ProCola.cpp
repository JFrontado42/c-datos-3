//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "ProCola.h"
#include "Cola.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
using namespace Persona;
TForm1 *Form1;
Pers *col;

void recurs(Pers *l,int j,TStringGrid* StringGrid1){
	if(!l->esvacioc(l))
        {
		StringGrid1->Cells[0][j]=l->nombre;
		StringGrid1->Cells[1][j]=l->ced;
		StringGrid1->Cells[2][j]=l->edad;
		recurs(l->sig,j+1,StringGrid1);
	}
        }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
void updatList(TStringGrid *StringGrid1){
	int tama=col->totalnodos(col,0)+1;
	StringGrid1->RowCount=tama;
	StringGrid1->Cells[0][0]="Nombre";
	StringGrid1->Cells[1][0]="Edad";
	StringGrid1->Cells[2][0]="Cedula";
	recurs(col,1,StringGrid1);
}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
void msgVacio(){
	Application->MessageBoxA("La lista esta vacia","Error",0);
         }
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
void msgModif(const char *txt1,const char *txt2)
{
	Application->MessageBoxA(txt1,txt2,0);
}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


void __fastcall TForm1::Edit1Click(TObject *Sender)
{
  	if(!Edit1->Modified){
		Edit1->Clear();
	}
        else
        {
		Edit1->SelectAll();
	}
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


void __fastcall TForm1::Edit2Click(TObject *Sender)
{
 	if(!Edit2->Modified)
        {
		Edit2->Clear();
	}
        else
        {
		Edit2->SelectAll();
	}
}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

void __fastcall TForm1::Edit3Click(TObject *Sender)
{
	if(!Edit3->Modified)
        {
		Edit3->Clear();
	}
        else
        {
		Edit3->SelectAll();
	}
}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
void __fastcall TForm1::Button1Click(TObject *Sender)
{
if(!Edit1->Modified||!Edit2->Modified||!Edit3->Modified){
		msgModif("No a introducido todos los datos, Porfavor introduzca los datos faltantes","Cambiar contenido");
        }else{  
                Pers *aux;
		aux=new Pers;
		aux->nombre=Edit1->Text;
		Edit1->Clear();
		aux->ced=Edit2->Text.ToInt();
		Edit2->Clear();
		aux->edad=Edit3->Text.ToInt();
		Edit3->Clear();
		aux->crear_nodo(aux,col);
                delete aux;
                updatList(StringGrid1);
                }
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button2Click(TObject *Sender)
{
	if(col->esvacioc(col)){
		msgVacio();
	}else{
		col->desencolar(col);
		updatList(StringGrid1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
    if(RadioButton1->Checked)
    {
       if(col->esvacioc(col))
       {
          msgVacio();
	}
        else
        {
		col->ordenar(col);
		updatList(StringGrid1);
	}
    }
    else
    {
       if(RadioButton2->Checked)
       {
       	if(col->esvacioc(col))
        {
		msgVacio();
	}
        else
        {
		col->menor(col);
		updatList(StringGrid1);
	}
       }
       else
       {
       if(RadioButton3->Checked){
       if(col->esvacioc(col)){
		msgVacio();
	}else{
		col->mayor(col);
		updatList(StringGrid1);
	}
        }
}
}
}
//---------------------------------------------------------------------------



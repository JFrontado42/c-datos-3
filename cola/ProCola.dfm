object Form1: TForm1
  Left = 338
  Top = 128
  Width = 587
  Height = 333
  Caption = 'Cola'
  Color = clBtnFace
  DockSite = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox3: TGroupBox
    Left = 240
    Top = 112
    Width = 329
    Height = 169
    Caption = 'Lista de Registrados'
    TabOrder = 7
    object StringGrid1: TStringGrid
      Left = 0
      Top = 16
      Width = 201
      Height = 153
      ColCount = 3
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      GridLineWidth = 2
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 577
    Height = 105
    Caption = 'Agregar Persona'
    TabOrder = 0
    object Label1: TLabel
      Left = 1
      Top = 24
      Width = 37
      Height = 13
      Caption = 'Nombre'
    end
    object Label4: TLabel
      Left = 209
      Top = 24
      Width = 25
      Height = 13
      Caption = 'Edad'
    end
    object Label5: TLabel
      Left = 385
      Top = 24
      Width = 33
      Height = 13
      Caption = 'Cedula'
    end
    object Button1: TButton
      Left = 224
      Top = 64
      Width = 121
      Height = 25
      Caption = 'Agregar'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Edit2: TEdit
      Left = 238
      Top = 24
      Width = 137
      Height = 21
      TabOrder = 2
      Text = 'Edad'
      OnClick = Edit2Click
    end
    object Edit3: TEdit
      Left = 422
      Top = 24
      Width = 137
      Height = 21
      TabOrder = 3
      Text = 'Cedula'
      OnClick = Edit3Click
    end
    object Edit1: TEdit
      Left = 46
      Top = 24
      Width = 137
      Height = 21
      TabOrder = 1
      Text = 'Nombre'
      OnClick = Edit1Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 448
    Top = 168
    Width = 113
    Height = 49
    Caption = 'Eliminar primer nodo'
    TabOrder = 1
    object Button2: TButton
      Left = 16
      Top = 16
      Width = 81
      Height = 25
      Caption = 'Eliminar'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object RadioGroup1: TRadioGroup
    Left = 0
    Top = 112
    Width = 233
    Height = 185
    Caption = 'Mostrar:'
    TabOrder = 2
  end
  object RadioButton1: TRadioButton
    Left = 8
    Top = 128
    Width = 201
    Height = 33
    Caption = 'Reporte ordenado Alfabeticamente'
    TabOrder = 3
  end
  object RadioButton2: TRadioButton
    Left = 8
    Top = 160
    Width = 161
    Height = 33
    Caption = 'Persona con menor Edad'
    TabOrder = 4
  end
  object RadioButton3: TRadioButton
    Left = 8
    Top = 192
    Width = 177
    Height = 33
    Caption = 'Persona con la mayor Cedula'
    TabOrder = 5
  end
  object Button3: TButton
    Left = 30
    Top = 240
    Width = 137
    Height = 33
    Caption = 'Mostrar'
    TabOrder = 6
    OnClick = Button3Click
  end
end

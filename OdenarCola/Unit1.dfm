object Form1: TForm1
  Left = 192
  Top = 114
  Width = 337
  Height = 395
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 329
    Height = 169
    Caption = 'Datos'
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 40
      Width = 16
      Height = 13
      Caption = 'C.I:'
    end
    object Label2: TLabel
      Left = 16
      Top = 72
      Width = 37
      Height = 13
      Caption = 'Nombre'
    end
    object Label3: TLabel
      Left = 16
      Top = 104
      Width = 28
      Height = 13
      Caption = 'Edad:'
    end
    object Edit1: TEdit
      Left = 64
      Top = 40
      Width = 81
      Height = 21
      TabOrder = 0
      Text = 'Cedula.'
      OnClick = Edit1Click
    end
    object Edit2: TEdit
      Left = 64
      Top = 72
      Width = 81
      Height = 21
      TabOrder = 1
      Text = 'Nombre.'
      OnClick = Edit2Click
    end
    object Edit3: TEdit
      Left = 64
      Top = 104
      Width = 81
      Height = 21
      TabOrder = 2
      Text = 'Edad.'
      OnClick = Edit3Click
    end
    object Button1: TButton
      Left = 184
      Top = 40
      Width = 113
      Height = 89
      Caption = 'Encolar'
      TabOrder = 3
      OnClick = Button1Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 168
    Width = 329
    Height = 193
    Caption = 'Cola'
    TabOrder = 1
    object Label4: TLabel
      Left = 216
      Top = 8
      Width = 102
      Height = 13
      Caption = 'Seleccione un criterio'
    end
    object Button2: TButton
      Left = 224
      Top = 128
      Width = 89
      Height = 57
      Caption = 'Desencolar'
      TabOrder = 0
      OnClick = Button2Click
    end
    object StringGrid1: TStringGrid
      Left = 8
      Top = 24
      Width = 201
      Height = 153
      ColCount = 3
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      GridLineWidth = 2
      TabOrder = 1
    end
    object ListBox1: TListBox
      Left = 232
      Top = 24
      Width = 81
      Height = 73
      ItemHeight = 13
      TabOrder = 2
    end
    object Button3: TButton
      Left = 248
      Top = 96
      Width = 49
      Height = 25
      Caption = 'Ordenar'
      TabOrder = 3
      OnClick = Button3Click
    end
  end
end

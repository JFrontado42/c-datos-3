//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Ordenar3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
using namespace Personas;
TForm1 *Form1;
Persona *Lista;

void Mostrar(Persona *k,int i,TStringGrid* StringGrid1){
	if(!k->EsVacio(k))
        {
		StringGrid1->Cells[0][i]=k->ced;
		StringGrid1->Cells[1][i]=k->Nombre;
		StringGrid1->Cells[2][i]=k->edad;
		Mostrar(k->Siguiente,i+1,StringGrid1);
	}
        }

void updatList(TStringGrid *StringGrid1){
int Tamano=Lista->Nodos(Lista,0)+1;
StringGrid1->RowCount=Tamano;
StringGrid1->Cells[0][0]="C.I:";
StringGrid1->Cells[1][0]="Nombre";
StringGrid1->Cells[2][0]="Edad";
Mostrar(Lista,1,StringGrid1);
}



void Faltan(const char *k,const char *s)
{
	Application->MessageBoxA(k,s,0);
}

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{

	ListBox1->Items->Add("Nombre");
	ListBox1->Items->Add("Edad");
	ListBox1->Items->Add("Cedula");

}
void __fastcall TForm1::Edit1Click(TObject *Sender)
{
    if(!Edit1->Modified){
		Edit1->Clear();
	}else{
		Edit1->SelectAll();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit2Click(TObject *Sender)
{
     if(!Edit2->Modified){
		Edit2->Clear();
	}else{
		Edit2->SelectAll();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit3Click(TObject *Sender)
{
          if(!Edit3->Modified){
		Edit3->Clear();
	}else{
		Edit3->SelectAll();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
     if(!Edit1->Modified||!Edit2->Modified||!Edit3->Modified){
     Application->MessageBoxA("Introduzca todos los datos","Faltan Datos",0);
        }else{
                Persona *aux;
		aux=new Persona;
		aux->ced=Edit1->Text.ToInt();
		Edit1->Clear();
		aux->Nombre=Edit2->Text;
		Edit2->Clear();
		aux->edad=Edit3->Text.ToInt();
		Edit3->Clear();
		aux->NodoNuevo(aux,Lista);
                delete aux;
                updatList(StringGrid1);
                }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
            if(Lista->EsVacio(Lista)){
		Application->MessageBoxA("Vacio","Error",0);
	}else{
		Lista->Desencolar(Lista);
		updatList(StringGrid1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
     switch (ListBox1->ItemIndex) {
   case 0: {
   if(Lista->EsVacio(Lista)){
          Application->MessageBoxA("Vacio","Error",0);
	}
        else
        {
		Lista->Ordenar(Lista);
		updatList(StringGrid1);
       	}
        break;
    }
   

    case 1: {
    if(Lista->EsVacio(Lista))
       {
          Application->MessageBoxA("Vacio","Error",0);
	}
        else
        {
		Lista->Menor(Lista);
		updatList(StringGrid1);
	}
    break;
    }


    case 2: {if(Lista->EsVacio(Lista))
       {
          Application->MessageBoxA("Vacio","Error",0);
	}
        else
        {
		Lista->Mayor(Lista);
		updatList(StringGrid1);
	}
    }
    break;
    }


}
//---------------------------------------------------------------------------


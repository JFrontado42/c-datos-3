#ifndef ColaCod
#define ColaCod
#include <cstdlib>
#include <vcl.h>

using namespace std;
namespace Personas{
class Persona{
private:

public:
int ced;
int edad;
AnsiString Nombre;
Persona *Siguiente;

Persona(){
        ced=0;
        edad=0;
        Nombre=" ";
        Siguiente=NULL;
}

bool EsVacio(Persona *k){
if(k==NULL){
return true;
}else{
return false;
}
}
int PrimeroCed(Persona *k){
if(!EsVacio(k)){
return k->ced;
}else{
return 0;
}
}
int PrimeroEdad(Persona *k){
if(!EsVacio(k)){
return k->edad;
}else{
return 0;
}
}
AnsiString PrimeroNombre(Persona *k){
if(!EsVacio(k)){
return k->Nombre;
}else{
return " ";
}
}
void NodoNuevo(Persona *aux,Persona *&k){
if(!EsVacio(k)){
NodoNuevo(aux,k->Siguiente);
}else{
k=new Persona;
*k=*aux;
}
}
void Desencolar(Persona *&k){
Persona *aux;
       aux=new Persona;
       	if (!EsVacio(k))
           {
			*aux=*k;
			if (!EsVacio(k->Siguiente))
            {
				*k = *k->Siguiente;
			}
                        else
            {
				k=NULL;
			}
		}
      delete aux;
}

void Encolar(Persona *&k, Persona *m)
      {
    	if (!EsVacio(k))
         {
                Encolar(k->Siguiente,m);
         }
         else
         {
               k=new Persona;
               *k=*m;
         }
	}

void Copiar(Persona *&s, Persona *n){
if(!EsVacio(n)){
Encolar(s,Valor(n));
}
}
Persona* Valor(Persona *n){
Persona *aux;
aux=new Persona;
*aux=*n;
aux->Siguiente=NULL;
return aux;
}

void Pasar(Persona *&o, Persona *d){
if(!EsVacio(o)){
Copiar(d,o);
Desencolar(o);
Pasar(o,d);
}
}

void Mayor(Persona *&o){
Persona *may,*may2;
may=NULL;
may2=NULL;
BuscarMayor(o,may,may2);
*o=*may;
delete may;
delete may2;
}
void BuscarMayor(Persona *r,Persona *&m,Persona *k){
if (!EsVacio(r)){
if(EsVacio(m)){
Copiar(m,r);
}else{
if(PrimeroCed(r)>PrimeroCed(m)){
Copiar(k,r);
Pasar(m,k);
*m=*k;
k=NULL;
}else{
Copiar(m,r);
}
}
Desencolar(r);
BuscarMayor(r,m,k);
}
}

void Menor(Persona *&o){
Persona *men;
Persona *men2;
men=NULL;
men2=NULL;
BuscarMenor(o,men,men2);
*o=*men;
delete men;
delete men2;
}

void BuscarMenor(Persona *r,Persona *&m,Persona *k){
if (!EsVacio(r)){
if(EsVacio(m)){
Copiar(m,r);
}else{
if(PrimeroCed(r)<PrimeroCed(m)){
Copiar(k,r);
Pasar(m,k);
*m=*k;
k=NULL;
}else{
Copiar(m,r);
}
}
Desencolar(r);
BuscarMenor(r,m,k);
}
}

void Ordenar(Persona *&o){
Persona *aux, *aux1;
aux=NULL;
aux1=NULL;
OrdenarNombre(o,aux,aux1);
*o=*aux;
delete aux;
delete aux1;
}

void OrdenarNombre(Persona *r,Persona *&m,Persona *k){
if(!EsVacio(r)){
if(EsVacio(m)){
Copiar(m,r);
}else{
if (PrimeroNombre(r)<PrimeroNombre(m)){
Copiar(k,r);
Pasar(m,k);
*m=*k;
k=NULL;
}else{
Copiar(k,m);
Desencolar(m);
if(EsVacio(m)){
Copiar(k,r);
*m=*k;
k=NULL;
}
else{
PasarMenor(r,m,k);
}
}
}
Desencolar(r);
OrdenarNombre(r,m,k);
}
}

void PasarMenor(Persona *h, Persona *j, Persona *&s){
if(!EsVacio(j)){
if(PrimeroNombre(j)<PrimeroNombre(h)){
Copiar(s,j);
Desencolar(j);
PasarMenor(h,j,s);
}else{
Copiar(s,h);
Pasar(j,s);
*j=*s;
s=NULL;
}
}else{
Copiar(s,h);
*j=*s;
s=NULL;
}
}

int Nodos(Persona *k,int i){
if (!EsVacio(k)){
return Nodos(k->Siguiente,i+1);
}else{
return i;
}
}

};
}

#endif

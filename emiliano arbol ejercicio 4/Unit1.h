//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cola.h"
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TButton *Cargar_ABB;
        void __fastcall Cargar_ABBClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif

class ABB {
    public:
        String valor;
        ABB *Der,*Izq;

    ABB CrearA (){
        ABB *aux;
        aux->valor=0;
        aux->Der=NULL;
        aux->Izq=NULL;
        return *aux;
    }
    bool EsvacioA (ABB *A){
        if(A==NULL){
            return true;
        }else{
            return false;
        }
    }
    String Valor(ABB *A){
        if (!EsvacioA(A)){
            return A->valor;
        }else{
            return 0;
        }
    }
    void CrearRaiz (ABB *&A, String I){
        ABB *Aux=new ABB();
        Aux->Izq=NULL;
        Aux->Der=NULL;
        Aux->valor=I;
        A=Aux;
    }
    void InsertarH (ABB *&A, String I){
        ABB *Aux=new ABB();
        Aux=A;
        if(EsvacioA(A)){
            CrearRaiz(A,I);
        }else{
            if(Valor(A)>I){
                InsertarH(Aux->Izq,I);
            }else{
                if(Valor(A)<I){
                    InsertarH(Aux->Der,I);
                }
            }
        }
    }

    bool Buscar (ABB *A, int I){
        if(!EsvacioA(A)){
                if(Valor(A)==I){
                        return true;
                }else{
                        if(Valor(A)>I){
                                return Buscar(A->Izq,I);
                        }else{
                                return Buscar(A->Der,I);
                        }
                }
        }else{
                return false;
        }
    }
    void EliminarH (ABB *&A, int I){
        ABB *aux,*aux1,*otro;
        bool bo;
        if(!EsvacioA(A)){
           if(Valor(A)>I){
               EliminarH(A->Izq,I);
           }else{
               if(Valor(A)<I){
                   EliminarH(A->Der,I);
               }else{
                   otro=A;
                   if(EsvacioA(otro->Der)){
                       A=otro->Izq;
                   }else{
                       if(EsvacioA(otro->Izq)){
                           A=otro->Der;
                       }else{
                           aux=A->Izq;
                           bo=false;
                           while(!EsvacioA(aux->Der)){
                               aux1=aux;
                               aux=aux->Der;
                               bo=true;
                           }
                           A->valor=aux->valor;
                           otro=aux;
                           if(bo){
                               aux1->Der=aux->Izq;
                           }else{
                               A->Izq=aux->Izq;
                           }
                       }
                   }
                   delete(otro);
                   otro=new ABB();
               }
           }
        }
    }
    int PreOrden(ABB *A){
        if(!EsvacioA(A)){
            PreOrden(A->Izq);
            //Raiz
            PreOrden(A->Der);
        }
    }
    void InOrden(ABB *A){
        if(!EsvacioA(A)){
            //Raiz
            InOrden(A->Izq);
            InOrden(A->Der);
        }
    }
    void PosOrden(ABB *A){
        if(!EsvacioA(A)){
            PosOrden(A->Izq);
            PosOrden(A->Der);
            //Raiz
        }
    }

        void Arbol (ABB *&A,ABB *C,ABB *B){
                ABB *Aux=new ABB();
                if(!EsvacioA(A)){
                        Aux->CrearRaiz(Aux,Valor(A));
                        if(!EsvacioA(C)){
                                Aux->Izq=C;
                        }
                        if(!EsvacioA(B)){
                                Aux->Der=B;
                        }
                        A= Aux;
                }
        }
        int obtenerElementos(Cola *&C, String E){
                if (C->Primero(C) == E){
                        C->Desencolar(C);
                        return 1;
                }else{
                        return 0;
                }
        }
        bool es_numero(String V,int cont){
                char *a;
                a=new char [50];
                a=V.c_str();
                if(int(a[cont])>=48 && int(a[cont])<=57 ){
                        cont++;
                        return es_numero(V,cont);
                }else{
                        if(a[cont]=='�'){
                                return true;
                        }else{
                                return false;
                        }
                }
        }
        ABB *obtenerNumero(Cola *&C){
                ABB *x;
                if (es_numero(C->Primero(C)+"�",0)==true){
                        CrearRaiz(x,C->Primero(C));
                        C->Desencolar(C);
                        Arbol (x, NULL, NULL);
                        return x;
                }else{
                        return NULL;
                }
        }
        ABB *obtenerProducto(Cola *&C){
                ABB *a,*b,*aux;
                aux=NULL;
                a = obtenerNumero(C);
                if (obtenerElementos(C, '*')){
                        b = obtenerProducto(C);
                        CrearRaiz(aux,'*');
                        Arbol (aux, a, b);
                        return aux;
                }else{
                        return a;
                }
        }
        ABB *obtenerSuma(Cola *&C){
                ABB *a,*b,*aux;
                aux=NULL;
                a = obtenerProducto(C);
                if (obtenerElementos(C, '+')){
                        b = obtenerSuma(C);
                        CrearRaiz(aux,'+');
                        Arbol (aux, a, b);
                        return aux;
                }else{
                        return a;
                }
        }


};


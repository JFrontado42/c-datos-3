object Form1: TForm1
  Left = 192
  Top = 114
  Width = 618
  Height = 488
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 120
    Top = 16
    Width = 37
    Height = 13
    Caption = 'Matriz 1'
  end
  object Label2: TLabel
    Left = 408
    Top = 16
    Width = 37
    Height = 13
    Caption = 'Matriz 2'
  end
  object Label3: TLabel
    Left = 192
    Top = 216
    Width = 37
    Height = 13
    Caption = 'Matriz 3'
  end
  object StringGrid1: TStringGrid
    Left = 64
    Top = 48
    Width = 201
    Height = 137
    ColCount = 1
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    TabOrder = 0
  end
  object StringGrid2: TStringGrid
    Left = 320
    Top = 48
    Width = 201
    Height = 137
    ColCount = 1
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    TabOrder = 1
  end
  object StringGrid3: TStringGrid
    Left = 152
    Top = 248
    Width = 209
    Height = 153
    ColCount = 1
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    TabOrder = 2
  end
  object Button1: TButton
    Left = 424
    Top = 280
    Width = 89
    Height = 49
    Caption = 'Multiplicar'
    TabOrder = 3
    OnClick = Button1Click
  end
end

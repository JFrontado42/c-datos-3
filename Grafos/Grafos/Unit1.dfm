object Form1: TForm1
  Left = 192
  Top = 114
  Width = 317
  Height = 395
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 312
    Width = 68
    Height = 13
    Caption = 'Costo  minimo:'
    Enabled = False
  end
  object Label2: TLabel
    Left = 8
    Top = 336
    Width = 68
    Height = 13
    Caption = 'Costo maximo:'
    Enabled = False
  end
  object Label3: TLabel
    Left = 96
    Top = 312
    Width = 49
    Height = 17
    Caption = 'Label3'
    Enabled = False
  end
  object Label4: TLabel
    Left = 96
    Top = 336
    Width = 49
    Height = 17
    Caption = 'Label4'
    Enabled = False
  end
  object Panel1: TPanel
    Left = 184
    Top = 24
    Width = 121
    Height = 113
    TabOrder = 5
    object Button3: TButton
      Left = 16
      Top = 72
      Width = 72
      Height = 25
      Caption = 'Analizar'
      Enabled = False
      TabOrder = 0
      OnClick = Button3Click
    end
  end
  object Edit1: TEdit
    Left = 48
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'Nodo 1'
    OnClick = Edit1Click
  end
  object Edit2: TEdit
    Left = 48
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'Nodo 2'
    OnClick = Edit2Click
  end
  object Button1: TButton
    Left = 200
    Top = 64
    Width = 73
    Height = 25
    Caption = 'Crear Camino'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 200
    Top = 32
    Width = 73
    Height = 25
    Caption = 'Insertar'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Edit3: TEdit
    Left = 48
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'Peso'
    OnClick = Edit3Click
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 152
    Width = 305
    Height = 153
    Caption = 'Grafo'
    TabOrder = 6
  end
end

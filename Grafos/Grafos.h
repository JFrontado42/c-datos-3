#ifndef GRAFOS_H_INCLUDED
#define GRAFOS_H_INCLUDED
//http://decsai.ugr.es/~jfv/ed1/tedi/cdrom/docs/grafos.htm

                 struct Arco{ //estructura para simular el comportamiento de un arco o arista del grafo
                int Peso;
                struct Nodo *Origen; //origen
                struct Nodo *Destino;
                struct Arco *Sig; //  l siguiente arco de la lista
                bool Estado;
                };
              struct Nodo{
                AnsiString Etiqueta;//nombre del nodo
                int Nodo;                            //numero
                struct Nodo *NSig;                           //al siguiente nodo
                struct Arco *Ady;                                               //a la lista de adyacencia del nodo
                bool Estado;     //recorrido o no                                                                                  //
                };

class Grafo{

    public:
 struct Nodo *Inicio;    //argh     //error?
  int NNodos;                     //Numero de nodos en el grafo

Grafo(){
    Inicio=new Nodo;
    Inicio->Etiqueta = 0;
    Inicio->Nodo = 1;
    Inicio->Estado=false;
    Inicio->Ady  = NULL;
    Inicio->NSig  = NULL;
    NNodos=NULL;
    }

Grafo* CrearRaiz(AnsiString Dat){
        Grafo *G=new Grafo();
        G->Inicio->Etiqueta=Dat;
        G->Inicio->Nodo=1;
        G->Inicio->Estado=false;
        G->NNodos=1;
        return G;
}

Nodo* EtiquetaBus(Nodo *n,AnsiString D){
        if(n!=NULL){
                if (n->Etiqueta==D){
                        return n;
                                    }else{
                                            return EtiquetaBus(n->NSig,D);
                                        }
                    }else{
                            return NULL;
                            }

}

void VertiseCero(Nodo *&n,AnsiString D){
        if(n!=NULL){
                if (n->Etiqueta==D){
                                    n-> Nodo=0;
                                    }else{
                                            VertiseCero(n->NSig,D);
                                            }
                    }
}

bool ExisteArco(Nodo *o, Nodo *d,Arco *a){
    if(a!=NULL){
                if ((a->Origen==o) && (a->Destino==d)){
                                                        return true;
                                                    }else{
                                                        return ExisteArco (o,d,a->Sig);
                                                            }
                    }else{
                            return false;
                            }
}

bool InsertarArco (Nodo *o, Nodo *d, int Peso){
        if(o!=NULL && d!=NULL){//para eso han de existir ambos nodos
                Arco *aux=new Arco();
                aux->Origen=o;
                aux->Destino=d;
                aux->Estado=false;
                aux->Peso=Peso;
                aux->Sig=o->Ady;          //EEror?
                o->Ady=aux;                       //error?
                return true;
        }else{
                return false;
        }
}

void InsertarNodo(Grafo *&G, AnsiString D, Nodo *o){
    if(G==NULL){
                G=G->CrearRaiz(D);
        }else{
                Nodo *aux;
                aux = new Nodo();
                if(o->NSig!=NULL){
                                InsertarNodo(G,D,o->NSig);
                                }else{
                                    aux->Etiqueta=D;
                                    aux->Nodo=o->Nodo+1;
                                    aux->Ady=NULL;
                                    aux->Estado=false;
                                    aux->NSig=NULL;
                                    o->NSig = aux;
                                    (this->NNodos)++;
                                    }
            }
}
Arco* PrimerArco(Nodo *n, Grafo *g){
   return(n->Ady);
}



Nodo* PrimerNodo(Grafo *g){
   return(g->Inicio->NSig);
}



void CrearNodo(Grafo *&g,AnsiString Dato){
        Grafo *aux; //variable auxiliar
        if(g==NULL){
                aux=CrearRaiz(Dato);
                g=new Grafo();
                g->Inicio=aux->Inicio;
                g->NNodos=aux->NNodos;
        }else{
                if(EtiquetaBus(g->Inicio,Dato)==NULL){
                        InsertarNodo(g,Dato,g->Inicio);
                }
        }
}
bool CrearArco(AnsiString o, AnsiString d,int Peso){
        return InsertarArco(EtiquetaBus(Inicio,o),EtiquetaBus(Inicio,d),Peso);

}

};
#endif // GRAFOS_H_INCLUDED

//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Dipolo.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
using namespace Dipolo;
TForm1 *Form1;
Palindromo *Dip;



void Cadena(AnsiString Cad,Palindromo *&k){
for (int i = 0; i <= Cad.Length(); i++){
Elemento *Aux;
Aux=new Elemento;
Aux->Nombre=Cad.c_str()[i];
Dip->PostInsertar(k,Aux);

}
}

bool Comparar(Palindromo *k){
if(!Dip->EsVacio(k)){
    return Dip->CompararRe(k->P,k->U);

}
}
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit1Click(TObject *Sender)
{
   if(!Edit1->Modified){
		Edit1->Clear();
	}else{
		Edit1->SelectAll();
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
if(!Edit1->Modified){
	       Application->MessageBoxA("Campo de lectura vacio.","Inserte una cadena.");
}else{
                Dip=new Palindromo;
                AnsiString a;
		a=Edit1->Text;
                Edit1->Clear();
                Cadena(a,Dip);
               //ShowMessage(Dip->Siguiente->Nombre);
               if(Comparar(Dip)){
                Application->MessageBoxA("Es palindromo.","Resultado.");
                }else{Application->MessageBoxA("No es palindromo.","Resultado.");}
      }
}
//---------------------------------------------------------------------------

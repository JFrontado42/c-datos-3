object Form1: TForm1
  Left = 591
  Top = 251
  Width = 169
  Height = 155
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = -16
    Top = 0
    Width = 177
    Height = 121
    Caption = 'Panel1'
    TabOrder = 0
    object Label1: TLabel
      Left = 40
      Top = 8
      Width = 93
      Height = 13
      Caption = 'Verificar Palindromo'
    end
    object Label2: TLabel
      Left = 40
      Top = 25
      Width = 92
      Height = 13
      Caption = 'Inserte una Palabra'
    end
    object Edit1: TEdit
      Left = 40
      Top = 48
      Width = 97
      Height = 21
      TabOrder = 0
      Text = 'Palabra'
      OnClick = Edit1Click
    end
    object Button1: TButton
      Left = 48
      Top = 80
      Width = 81
      Height = 25
      Caption = 'Comprobar'
      TabOrder = 1
      OnClick = Button1Click
    end
  end
end

#ifndef tDipolo
#define tDipolo
#include <cstdlib>
#include <vcl.h>
#include "Elemento1.h"
using namespace std;
using namespace Elementos;

namespace Dipolo{
class Palindromo{
private:

public:

Elemento *P;
Elemento *U;

Palindromo(){
P=NULL;
U=NULL;
}

bool EsVacio(Palindromo *d){
if((d->P==NULL)&&(d->U==NULL)){  //que sera lo que pasa?
return true;
}else{
return false;
}
}

bool EsVacioE(Elemento *d){
if(d==NULL){  //que sera lo que pasa?
return true;
}else{
return false;
}
}
AnsiString Primero(Palindromo *d){
if(!EsVacio(d)){
return d->P->Nombre;
}else{
return " ";
}
}

AnsiString Ultimo(Palindromo *d){
if(!EsVacio(d)){
return d->U->Anterior->Nombre;}else{ //esto me llevo un buen rato
return " ";
}
}

void PreInsertar(Palindromo *&d,Elemento *&e){
if(EsVacio(d)){
d->P=e;
d->U=e;
}else{
e->Siguiente=d->P;
d->P->Anterior=e;      //error?
d->P=e;}
}

void PostInsertar(Palindromo *&d,Elemento *&e){
if(EsVacio(d)){
d->P=e;
d->U=e;
}else{
e->Anterior=d->U;
d->U->Siguiente=e;   //error?
d->U=e;}
}

void PreEliminar(Palindromo *&d){
if(!EsVacio(d)){
if(d->U==d->P){
delete d->P;
d->P=NULL;
d->U=NULL;}else{
d->P=d->P->Siguiente;      //error?
delete d->P->Anterior;        //error?
d->P->Anterior=NULL;
}}
}

void PostEliminar(Palindromo *&d){
if(!EsVacio(d)){
if(d->U==d->P){
delete d->P;
d->P=NULL;
d->U=NULL;
}else{
d->U=d->U->Anterior;
delete d->U->Siguiente;
d->U->Siguiente=NULL;}}
}


bool CompararRe(Elemento *AuxP,Elemento *AuxU){
if(!EsVacioE(AuxP)&&!EsVacioE(AuxU)){
if(AuxP==AuxU->Anterior){
return true;
}else{
if(AuxP->Nombre==AuxU->Anterior->Nombre){
return CompararRe(AuxP->Siguiente,AuxU->Anterior);
}else{
return false;
}
}
}else{
return false;
}
}


};
}

#endif

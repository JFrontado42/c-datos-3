#ifndef AVL
#define AVL
#include <cstdlib>
#include <vcl.h>

class arbolAVL{
public:
	int dato;
	arbolAVL *izq;
	arbolAVL *der;

	arbolAVL *hacer (int x, arbolAVL * izq, arbolAVL * der){
		arbolAVL* aux;
		aux=new arbolAVL;
		aux->dato=x;
		aux->izq=izq;
		aux->der=der;
		return aux;
	}

	bool es_vacio (arbolAVL *t){
		if(t==NULL)
			return true;
		else
			return false;
	}

	arbolAVL *izquierdo (arbolAVL* t){
		if(!es_vacio(t))
			return t->izq;
		else
			return NULL;
	}

	arbolAVL *derecho (arbolAVL* t){
		if(!es_vacio(t))
			return t->der;
		else
			return NULL;
	}

	int raiz (arbolAVL * t){
		if(!es_vacio(t))
			return t->dato;
		else
			return 0;
	}

	int mayor(int a, int b){
		if(a<b){
			return b;
		}else{
			return a;
		}
	}
	int altura(arbolAVL *A){
		if(es_vacio(A)){
			return 0;
		}else{
			return 1 +(mayor(altura(A->izq),altura(A->der)));
		}
	}

	void rotar_s (arbolAVL *&t, bool izq){
		arbolAVL *t1;
		if (izq){
			t1 = izquierdo(t);
			t->izq = derecho(t1);
			t1->der = t;
		}else{
			t1 = derecho(t);
			t->der = izquierdo(t1);
			t1->izq = t;
		}
		t = t1;
	}

	void rotar_d (arbolAVL *&t, bool izq){
		if (izq){
			rotar_s (t->izq, false);
			rotar_s (t, true);
		}else{
			rotar_s (t->der, true);
			rotar_s (t, false);
		}
	}

	void balancear (arbolAVL *&t){
		if(!es_vacio(t)){
			if (altura (izquierdo(t)) - altura (derecho(t)) == 2){
				if (altura (t->izq->izq) >= altura (t->izq->der)){
					ShowMessage("Rotacion Simple a la izquierda");
					rotar_s (t, true);
				}else{
					ShowMessage("Rotacion Doble a la izquierda");
					rotar_d (t, true);
				}
			}else if (altura (derecho(t)) - altura (izquierdo( t)) == 2){
				if (altura (t->der->der) >= altura (t->der->izq)){
					ShowMessage("Rotacion Simple a la derecha");
					rotar_s (t, false);
				}else{
					ShowMessage("Rotacion Doble a la derecha");
					rotar_d (t, false);
				}
			}
		}
	}

	void insertar (arbolAVL *&t, int x){
		if(!Buscar(t,x)){
			if (es_vacio (t))
				t = hacer (x, NULL, NULL);
			else{
				if (x < raiz (t))
					insertar (t->izq, x);
				else
					insertar (t->der, x);
			}
			balancear (t);
		}else
			ShowMessage("repetido");
	}

	void eliminar (arbolAVL *&t, int x){    *t;
		arbolAVL *aux;
		if (x < raiz(t))
			eliminar (t->izq, x);
		else if (x > raiz (t))
			eliminar (t->der, x);
		else{
			if (es_vacio (izquierdo (t)) && es_vacio (derecho (t))){
				free (t);
				t = NULL;
			}
			else if (es_vacio (izquierdo (t))){
				aux = t;
				t = t->der;
				free (aux);
			}
			else if (es_vacio (derecho (t))){
				aux = t;
				t = t->izq;
				free (aux);
			}else{
				t->dato = eliminar_min (t->der);
			}
		}
		balancear (t);
	}

	int eliminar_min (arbolAVL *&t){
		if (!es_vacio (t)){
			if (!es_vacio (izquierdo (t))){
				int x = eliminar_min (t->izq);
				balancear (t);
				return x;
			}else{
				arbolAVL *aux = t;
				int x = raiz (aux);
				t = derecho (t);
				free (aux);
				balancear (t);
				return x;
			}
		}
	}

	bool Buscar (arbolAVL *A, int I){
		if(!es_vacio(A)){
			if(A->dato==I){
				return true;
			}else{
				if(A->dato>I){
					return Buscar(A->izq,I);
				}else{
					return Buscar(A->der,I);
				}
			}
		}else{
			return false;
		}
	}

};
#endif

//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "cola.h"
#include "arbolAVL.h"
#include "arbolAVL2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
arbolAVL *B;
Cola *C;


void actualizVista(TTreeView *a,arbolAVL*b,int pos){
	if(!b->es_vacio(b)){
		if(pos==0){
			a->Items->AddChild(NULL,b->dato);
		}else{
			int i=0;
			while(a->Items->operator [](i)->Text!=AnsiString(pos)){
				i++;
			}
			a->Items->AddChild(a->Items->operator [](i),b->dato);
		}
		actualizVista(a,b->izq,b->dato);
		actualizVista(a,b->der,b->dato);
	}
}
void actualiza(TTreeView *a){
	a->Items->Clear();
	actualizVista(a,B,0);
	a->AutoExpand=true;
}



//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::AlturaClick(TObject *Sender)
{
	Label1->Caption=AnsiString(B->altura(B));

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Edit1KeyPress(TObject *Sender, char &Key)
{
	if (!((Key >= '0' && Key <= '9') || Key == '\b')){
		Key = 0;
	}

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Cargar_ColaClick(TObject *Sender)
{
	B->insertar(B,StrToInt(AnsiString(Edit1->Text)));
	Edit1->Clear();
	actualiza(TreeView1);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Cargar_ABBClick(TObject *Sender)
{
	B=NULL;
	actualiza(TreeView1);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Edit2KeyPress(TObject *Sender, char &Key)
{
	if (!(Key >= '0' && Key <= '9')|| Key != '\b')
		Key = 0;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button1Click(TObject *Sender)
{
	if(B->Buscar(B,StrToInt(Edit2->Text)))
		B->eliminar(B,StrToInt(Edit2->Text));
	else
		ShowMessage("El nodo no existe");
	actualiza(TreeView1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
	if(B->Buscar(B,StrToInt(Edit3->Text)))
		Label3->Caption="Encontrado";
	else
		Label3->Caption="No Existe";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{                *B;
	actualiza(TreeView1);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Edit1Click(TObject *Sender)
{
	Edit1->Clear();
}
//---------------------------------------------------------------------------




object Form1: TForm1
  Left = 214
  Top = 109
  Width = 661
  Height = 514
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 88
    Top = 128
    Width = 3
    Height = 13
  end
  object Altura_ABB: TLabel
    Left = 24
    Top = 128
    Width = 50
    Height = 13
    Caption = 'Altura AVL'
  end
  object Label2: TLabel
    Left = 128
    Top = 168
    Width = 33
    Height = 13
    Caption = 'Buscar'
  end
  object Label3: TLabel
    Left = 176
    Top = 168
    Width = 3
    Height = 13
  end
  object Altura: TButton
    Left = 24
    Top = 96
    Width = 75
    Height = 25
    Caption = 'altura'
    TabOrder = 0
    OnClick = AlturaClick
  end
  object Edit1: TEdit
    Left = 24
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 1
    OnClick = Edit1Click
    OnKeyPress = Edit1KeyPress
  end
  object Cargar_Cola: TButton
    Left = 152
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Agregar'
    TabOrder = 2
    OnClick = Cargar_ColaClick
  end
  object Cargar_ABB: TButton
    Left = 152
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Limpiar'
    TabOrder = 3
    OnClick = Cargar_ABBClick
  end
  object Button1: TButton
    Left = 32
    Top = 216
    Width = 75
    Height = 25
    Caption = 'EliminarH'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Edit2: TEdit
    Left = 24
    Top = 192
    Width = 89
    Height = 21
    TabOrder = 5
    OnKeyPress = Edit1KeyPress
  end
  object Button2: TButton
    Left = 136
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Buscar'
    TabOrder = 6
    OnClick = Button2Click
  end
  object Edit3: TEdit
    Left = 128
    Top = 192
    Width = 89
    Height = 21
    TabOrder = 7
    OnKeyPress = Edit1KeyPress
  end
  object TreeView1: TTreeView
    Left = 244
    Top = 0
    Width = 409
    Height = 480
    Align = alRight
    AutoExpand = True
    BevelOuter = bvRaised
    HideSelection = False
    Indent = 19
    TabOrder = 8
  end
  object Button3: TButton
    Left = 80
    Top = 352
    Width = 75
    Height = 25
    Caption = 'Actualizar'
    TabOrder = 9
    OnClick = Button3Click
  end
end

//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TButton *Altura;
        TEdit *Edit1;
        TButton *Cargar_Cola;
        TButton *Cargar_ABB;
        TLabel *Altura_ABB;
        TButton *Button1;
        TEdit *Edit2;
        TLabel *Label1;
        TButton *Button2;
        TLabel *Label2;
        TLabel *Label3;
        TEdit *Edit3;
	TTreeView *TreeView1;
	TButton *Button3;
        void __fastcall AlturaClick(TObject *Sender);
        void __fastcall Edit1KeyPress(TObject *Sender, char &Key);
        void __fastcall Cargar_ColaClick(TObject *Sender);
        void __fastcall Cargar_ABBClick(TObject *Sender);
        void __fastcall Edit2KeyPress(TObject *Sender, char &Key);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Edit1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif


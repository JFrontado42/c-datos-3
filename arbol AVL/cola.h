#ifndef colaH
#define colaH
class Cola {
public:
	int Num;
	Cola *sig;
	Cola(){
		Num=0;
		sig=NULL;
	}
	Cola *CrearNodo(int);
	Cola *PrimerNodo(Cola*);
	void Encolar(Cola *&K,Cola *P);
	bool EsvacioC(Cola *E);
	int PrimeroNum(Cola *K);
	void Desencolar (Cola *&K);
	void copiar(Cola *&S, Cola *N);
	void Pasar(Cola *&O, Cola *&D);
};
Cola *Cola::CrearNodo(int _Num){
	Cola *aux=new Cola();
	aux->sig=NULL;
	aux->Num=_Num;
	return aux;
}
bool Cola::EsvacioC(Cola *E){
	if(E==NULL){
		return true;
	}else{
		return false;
	}
}
Cola *Cola::PrimerNodo(Cola *P){
	Cola *aux=new Cola();
	aux->sig=NULL;
	aux->Num=P->Num;
	return aux;
}
void Cola::Encolar(Cola *&K,Cola *P){
	if(EsvacioC(K)){
		K=P;
	}else{
		if(K->sig==NULL){
			K->sig=P;
		}else{
			Encolar(K->sig,P);
		}
	}
}
int Cola::PrimeroNum(Cola *K){
	if (!EsvacioC(K)){
		return K->Num;
	}else{
		return 0;
	}
}
void Cola::Desencolar(Cola *&K){
	if(!EsvacioC(K)){
		if(K->sig==NULL){
			K=NULL;
		}else{
			K=K->sig;
		}
	}
}
void Cola::copiar(Cola *&S, Cola *N){
	if(!EsvacioC(N)){
		Encolar(S,N);
	}
}
void Cola::Pasar(Cola *&O, Cola *&D){
	if(!EsvacioC(O)){
		copiar(D,O);
		Desencolar(O);
		Pasar(O,D);
	}
}

void CargarC(Cola *&C,char Texto[],int cont){
	if(cont<strlen(Texto)){
		C->Encolar(C,C->CrearNodo(Texto[cont]-48));
		cont++;
		CargarC(C,Texto,cont);
	}
}



#endif

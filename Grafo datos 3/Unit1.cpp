//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "File1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
Grafo *G;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}

void actualizar2(Tarea *ta,int i,TStringGrid*&str){
	if(ta!=NULL){
		str->Cells[0][i]=ta->etiq;
		str->Cells[2][i]=AnsiString(ta->duracion);
		prela*aux=new prela;
		aux=ta->prelacion;
		AnsiString auxpre="";
		if(aux!=NULL){
			while (aux!=NULL){
				if(aux->sig==NULL){
					auxpre.operator +=(aux->nombre);
				}else{
					auxpre.operator +=(aux->nombre+",");
				}
				aux=aux->sig;
			}
			str->Cells[1][i]=auxpre;
		}else{
			str->Cells[1][i]="-";
		}
		str->Cells[3][i]=ta->dest->early;
		str->Cells[4][i]=ta->dest->last;
		actualizar2(ta->sig,i+1,str);
	}
}

void contarTareas(Tarea*ta,int &i){
	if(ta!=NULL){
		i++;
		contarTareas(ta->sig,i);
	}
}

void actualizar(TStringGrid*&str){
	int i=0;
	contarTareas(G->start,i);
	str->RowCount=i+1;
	str->Cells[0][0]="Actividad";
	str->Cells[1][0]="Prelacion";
	str->Cells[2][0]="Duracion";
	str->Cells[4][0]="Early";
	str->Cells[3][0]="Last";
	actualizar2(G->start,1,str);
	i=0;
}

//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn1Click(TObject *Sender)
{
	if (LabeledEdit1->Text==""|| LabeledEdit3->Text==""){
		ShowMessage("Debe llenar los recuadros de por lo menos actividad y duracion");
	}else{
		struct Tarea *aux=new Tarea;
		aux=G->crearTarea();
		aux->duracion=LabeledEdit3->Text.ToInt();
		aux->etiq=LabeledEdit1->Text;
		if(LabeledEdit2->Text!=""){
			if(G->insertarTarea(G,LabeledEdit2->Text,aux)){
				ShowMessage("La tarea se ha almacenado correctamente");
				LabeledEdit1->Clear();
				LabeledEdit2->Clear();
				LabeledEdit3->Clear();
			}else{
				ShowMessage("La tarea ya existe");
				LabeledEdit1->Clear();
			}
		}else{
			if((G->insertarTarea(G,"",aux))){
				ShowMessage("La tarea se ha almacenado correctamente");
				LabeledEdit1->Clear();
				LabeledEdit2->Clear();
				LabeledEdit3->Clear();
			}else{
				ShowMessage("La tarea ya existe");
				LabeledEdit1->Clear();
			}
		}
	G->conectarTareas(G->start);
	actualizar(StringGrid1);
	if(G->buscarTarea(G->start,"u")!=NULL){
		BitBtn1->Enabled=false;
	}
	}
}
//---------------------------------------------------------------------------




void __fastcall TForm1::FormCreate(TObject *Sender)
{
	G=NULL;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::LabeledEdit1KeyPress(TObject *Sender, char &Key)
{
	//validar letras y borrar
	if (!((int(Key) >= 97 && Key <= 122) || (Key == '\b'))){
		Key = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::LabeledEdit2KeyPress(TObject *Sender, char &Key)
{
	//validar comas y letras
	if ((int(Key) >= 0 && int(Key) <= 7)||(int(Key) >= 9 && int(Key) <= 43)||(int(Key) >= 45 && int(Key) <= 96)||(int(Key) >= 123 && int(Key) <= 126)){
		Key = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::LabeledEdit3KeyPress(TObject *Sender, char &Key)
{
	//validar numeros
	if (!(Key == '\b' || (Key >= '0'&&Key<='9'))){
		Key = 0;
	}
}
//---------------------------------------------------------------------------

void graficar(Graphics::TCanvas* canvas,Tarea*ta){
	if(ta!=NULL){
		Graphics::TBitmap *lol = new Graphics::TBitmap;
		lol->LoadFromFile("Dibujo.bmp");
		if(ta==G->start){
			canvas->Draw(ta->orig->pos.xOrig,ta->orig->pos.yOrig,lol);
			canvas->TextOutA(ta->orig->posid.xOrig,ta->orig->posid.yOrig,AnsiString(ta->orig->id));
			canvas->TextOutA(ta->orig->posear.xOrig,ta->orig->posear.yOrig,AnsiString(ta->orig->early));
			canvas->TextOutA(ta->orig->poslast.xOrig,ta->orig->poslast.yOrig,AnsiString(ta->orig->last));
			canvas->Draw(ta->dest->pos.xOrig,ta->dest->pos.yOrig,lol);
			canvas->TextOutA(ta->dest->posid.xOrig,ta->dest->posid.yOrig,AnsiString(ta->dest->id));
			canvas->TextOutA(ta->dest->posear.xOrig,ta->dest->posear.yOrig,AnsiString(ta->dest->early));
			canvas->TextOutA(ta->dest->poslast.xOrig,ta->dest->poslast.yOrig,AnsiString(ta->dest->last));
		}else{
			canvas->Draw(ta->dest->pos.xOrig,ta->dest->pos.yOrig,lol);
			canvas->TextOutA(ta->dest->posid.xOrig,ta->dest->posid.yOrig,AnsiString(ta->dest->id));
			canvas->TextOutA(ta->dest->posear.xOrig,ta->dest->posear.yOrig,AnsiString(ta->dest->early));
			canvas->TextOutA(ta->dest->poslast.xOrig,ta->dest->poslast.yOrig,AnsiString(ta->dest->last));
		}
		delete lol;
		graficar(canvas,ta->sig);
	}
}

void __fastcall TForm1::FormPaint(TObject *Sender)
{
/*	Graphics::TBitmap *lol = new Graphics::TBitmap;
	lol->LoadFromFile("Dibujo.bmp");
//	Canvas->MoveTo(136,30);
//	Canvas->LineTo(230,30);
//	Canvas->MoveTo(136,30);
//	Canvas->LineTo(230,120);
//	Canvas->Draw(200,90,lol);
//	Canvas->Draw(200,0,lol);
	Canvas->Draw(120,0,lol);
	Canvas->TextOutA(122,22,"aca");
	Canvas->TextOutA(152,12,"earl");
	Canvas->TextOutA(152,32,"last");
*/
}
AnsiString camino(Tarea *ta,AnsiString &cam){
	if(ta!=NULL){
		if(ta->orig->early==ta->orig->last){
			if(ta->sig==NULL){
				cam.operator +=(ta->etiq);
			}else{
				cam.operator +=(ta->etiq+",");
				camino(ta->sig,cam);
			}
		}else{
			camino(ta->sig,cam);
		}
	}
	return cam;
	
}


//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
	if(G!=NULL){
		if(G->buscarTarea(G->start,"u")!=NULL){
			G->conectarTareas(G->start);
			G->enumerarNodos(G->start,0);
			G->tiempoLast(G->start);
			actualizar(StringGrid1);
			Label2->Caption=camino(G->start,"");
			//G->actualizarGrafico(G->start,G->start->orig->pos);
			//graficar(Canvas,G->start);
		}else{
			ShowMessage("Debe insertar la tarea u para finalizar el proceso");
		}
	}else{
		ShowMessage("El grafo esta vacio, por favor introduzca informacion");
	}
}
//---------------------------------------------------------------------------


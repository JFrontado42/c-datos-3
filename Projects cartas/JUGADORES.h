#ifndef JUG_h
#define JUG_h

#include "cartasPI.h"
#include <cstdlib>
#include <ctime>


class jugadorCO{//jugadores
public:
	AnsiString nombre;
	int puntos;
	cartasPI *Abajo;//para mano
	jugadorCO *Sig;

jugadorCO(){                //instanciador del objeto jugador
	nombre="jugador";
	puntos=0;
	Abajo=NULL;
	Sig=NULL;
}
//--------------------------------------------es vacio-------------------------------------------------------
bool esvacio(jugadorCO *a){
	if (a==NULL){
		return true;
	}else{
		return false;
	}
}
//---------------------------------------------encolar--------------------------------------------------------
void encolar(jugadorCO *&a, jugadorCO *b) {
	if (!esvacio(a)) {
		encolar(a->Sig,b);
	} else {
		a=new jugadorCO;
		*a=*b;
	}
}
//---------------------------------------------desencolar-----------------------------------------------------
void desencolar(jugadorCO *&a) {
	if (!a->esvacio(a)) {
		if (!a->esvacio(a->Sig)){
			a = a->Sig;
		}else{
			a=NULL;
		}
	}
}
//--------------asigna las 4 cartas primeras del mazo a la cola de jugadores--------------
void AsignarCartas(jugadorCO *&e,cartasDI *&k){      //pasa las cartas de dipolo a pila de la mano del jugador
	if (!esvacio(e)){
		e->Abajo->Apilar(e->Abajo,e->Abajo->TransNodo(k));
		k->Preliminar(k);
		//elimina el primer elemento del dipolo mazo
		AsignarCartas(e->Sig,k); //esto va a asignar las primeras 4 cartas del mazo a los 4 jugadores y sale del ciclo
	}
}
//--------Recorre la lista de jugadores una y otra vez hasta que el mazo este vacio-------
void Repartir(jugadorCO *&e, cartasDI *&k){  *e;
	if (!k->Esvacio(k)){
		AsignarCartas(e,k); //reparte 4 cartas
		Repartir(e,k); //otra vuelta hasta que el mazo sea vacio//eto probablemente sea un error!!!
	}
}
//--------------------------Nuevo Jugador Con Un nombre pasado por parametro--------------
jugadorCO* nvo(AnsiString a){
	jugadorCO *aux;
	aux=new jugadorCO;
	aux->nombre=a;
	return aux;
}
};

#endif

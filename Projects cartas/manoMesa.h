#ifndef manoME_SA
#define manoME_SA

#include <cstdlib>
#include <ctime>

class manoMesa{
public:
	int Pinta,Valor;
	AnsiString Nombre;
	manoMesa *Ant;

	manoMesa(){
		Pinta=0;
		Valor=0;
		Nombre="";
		Ant=NULL;
	}
//-----------------------------------Es vacio---------------------------------------------
	bool esvacio(manoMesa *a){
		if(a==NULL){
			return true;
		}else{
			return false;
		}
	}
//------------------------------------Apilar----------------------------------------------
	void apilar(manoMesa *&a,manoMesa*b){
		if(!esvacio(a)){
			b->Ant=a;
			a=b;
		}else{
			a=b;
		}
	}
//-----------------------------------Mostrar la carta mayor-------------------------------
	manoMesa* tomarmayor(manoMesa*a,manoMesa *b){
		if(!esvacio(a)&&!esvacio(b)){
			if(a->Pinta==b->Pinta){
				if(a->Valor>b->Valor){
					manoMesa *aux;
					aux=new manoMesa;
					aux->Valor=a->Valor;
					aux->Pinta=a->Pinta;
					aux->Nombre=a->Nombre;
					return aux;
				}else{
					manoMesa *aux;
					aux=new manoMesa;
					aux->Valor=b->Valor;
					aux->Pinta=b->Pinta;
					aux->Nombre=b->Nombre;
					return aux;
				}
			}else{
				manoMesa *aux;
				aux=new manoMesa;
				aux->Valor=a->Valor;
				aux->Pinta=a->Pinta;
				aux->Nombre=a->Nombre;
				return aux;
			}
		}else{
			if(!esvacio(a)&& esvacio(b)){
				manoMesa *aux;
				aux=new manoMesa;
				aux->Valor=a->Valor;
				aux->Pinta=a->Pinta;
				aux->Nombre=a->Nombre;
				return aux;
			}else{
				manoMesa *aux;
				aux=new manoMesa;
				aux->Valor=b->Valor;
				aux->Pinta=b->Pinta;
				aux->Nombre=b->Nombre;
				return aux;
			}
		}
	}
//----------------------------------------------------------------------------------------
int ultimo(manoMesa *b){
	if(!esvacio(b)){
		if(b->esvacio(b->Ant)){
			return b->Pinta;
		}else{
			return ultimo(b->Ant);
		}
	}
}
//----------------------------------------------------------------------------------------
void bucar2(cartasPI*&a,int c,manoMesa*&b){
	if(!a->esvacioPi(a)){
		if(a->pinta==c){//si es igual a pinta
			if(b->Pinta!=c){
				b->Valor=a->valor;
				b->Pinta=a->pinta;
			}
			if(a->valor >b->Valor){//se toma la carta mas alta de esa pinta en la mano
				b->Valor=a->valor;
				b->Pinta=a->pinta;
			}
		}else{
			if(b->Pinta!=c){//si ya tiene un carta de esa pinta no hace esto simplemente lo salta
				if(a->pinta>=b->Pinta){//aqui deberia elegira la carta mas debil
					if(b->Valor==0&&b->Pinta==0){
						b->Valor=a->valor;
						b->Pinta=a->pinta;
					}
					if(a->valor<b->Valor){
						b->Valor=a->valor;
						b->Pinta=a->pinta;
					}
				}
			}
		}
		bucar2(a->Abajo,c,b);
	}
}
//----------------------------------------------------------------------------------------
void lanzaralgo(cartasPI *&a,manoMesa *&b){  *a;*b;
	if(b->Pinta==0&&b->Valor==0){
		b->Pinta=a->pinta;
		b->Valor=a->valor;
	}
	if(!a->esvacioPi(a)){//sino esvacia la pila
		if(a->pinta<b->Pinta){//evalua las pintas dandole prioridad a las menores
			if(a->valor>b->Valor){
				b->Valor=a->valor;//asignaciones
				b->Pinta=a->pinta;
			}
		}else{
			if((a->pinta+a->valor)>b->Pinta+b->Valor){/*Esto es por si tiene unas cartas de
			valor alto pero diferente de picas*/
				b->Valor=a->valor;
				b->Pinta=a->pinta;
			}
		}
		lanzaralgo(a->Abajo,b);
	}
}
//----------------------------------------------------------------------------------------
void desbuscar2(cartasPI*&a,manoMesa*b,cartasPI*&aux){
	if(!a->esvacioPi(a)){
		if(a->valor==b->Valor&&a->pinta==b->Pinta){
			a->desapilar(a);//aqui se pierde la info
		}else{
			a->Apilar(aux,a-> primeracarta(a));
			a->desapilar(a);//desapila a
		}
		desbuscar2(a,b,aux);//si es igual se lo salta
	}else{
		a=aux;// A VACIO SE LE ASIGNA AUX
	}
}
//----------------------------------------------------------------------------------------
void desbuscar(cartasPI*&a,manoMesa*b){
	desbuscar2(a,b,NULL);
}

};

#endif

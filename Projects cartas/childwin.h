//----------------------------------------------------------------------------
#ifndef ChildWinH
#define ChildWinH
//----------------------------------------------------------------------------
#include <vcl\Controls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\Classes.hpp>
#include <vcl\Windows.hpp>
#include <vcl\System.hpp>
#include <StdCtrls.hpp>
#include <ExtCtrls.hpp>
#include "Unit1.h"
#include <Grids.hpp>
#include <jpeg.hpp>
#include <Buttons.hpp>
//----------------------------------------------------------------------------
class TMDIChild : public TForm
{
__published:
	TPanel *Panel1;
	TButton *Inicio;
	TButton *Button1;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TButton *Button2;
	TStringGrid *GridSur;
	TStringGrid *GridEste;
	TStringGrid *GridNorte;
	TStringGrid *GridOeste;
	TLabel *Label5;
	TStringGrid *StringGrid1;
	TLabel *Label6;
	TImage *Image1;
	TBitBtn *BitBtn1;
	TLabel *Label7;
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall InicioClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall BitBtn1Click(TObject *Sender);
private:
public:
	virtual __fastcall TMDIChild(TComponent *Owner);
};
//----------------------------------------------------------------------------
#endif	

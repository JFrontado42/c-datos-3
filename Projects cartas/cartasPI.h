#ifndef cartas_PI
#define cartas_PI

#include <cstdlib>
#include <ctime>
///---------------------------------------------------------------------------------------

///---------------------------------------------------------------------------------------
class cartasPI{
public:
	cartasPI *Abajo;//declaracion de la .ant MEJORADA
	int valor,pinta;

cartasPI(){
	Abajo=NULL;
	valor=0;
	pinta=0;
}
//---------------------------------------es vacio pila--------------------------------------------------------
bool esvacioPi(cartasPI *k){
	if (k==NULL){
		return true;
	}else{
		return false;
	}
}
//-------------------------------------------apilar-----------------------------------------------------------
void Apilar(cartasPI *&e, cartasPI *k){//para apilar na mas se pasa el jugador con el *Abajo
	if (!esvacioPi(k)){
		if (!esvacioPi(e)){
			if (esvacioPi(k)){
				k=new cartasPI;
			}
			k->Abajo=e;
		}
		e=k;
	}
}
//---------------------------------------------desapilar------------------------------------------------------
void desapilar(cartasPI *&k){
	if (!esvacioPi(k)){
		if (!esvacioPi(k->Abajo)){
			cartasPI *aux;
			aux=new cartasPI;
			aux=k;
			k=k->Abajo;
			delete aux;
		}else{
			k=NULL;
		}
	}
}
//-----------------------------------de dipolo a pila---------------------------------------------------------
cartasPI* TransNodo(cartasDI *d){     //metodo que transforma un dipolo de cartas en pila de cartas
	cartasPI *aux;
	aux=new cartasPI;
	if (!d->Esvacio(d)){
		aux->pinta=d->P->pinta;
		aux->valor=d->P->valor;
		aux->Abajo=NULL;
	}else{
		aux=NULL;
	}
	return aux;
}
cartasPI* primeracarta(cartasPI*a){  //muestra el tope del mazo
	if(!esvacioPi(a)){
		cartasPI *aux;
		aux=new cartasPI;
		aux->valor=a->valor;
		aux->pinta=a->pinta;
		return aux;
	}else{
		return NULL;
	}
}
};

#endif

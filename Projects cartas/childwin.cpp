//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ChildWin.h"
#include "CARTAS.h"
#include "JUGADORES.h"
#include "manoMesa.h"
#include "cartasPI.h"
#include "cartasPI.h"


//---------------------------------------------------------------------
#pragma link "Unit1"
#pragma resource "*.dfm"

cartasDI *cartasDipolo;
jugadorCO *jugadores;
manoMesa *mesa;
manoMesa *e;

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

void ganadorPuntos(jugadorCO *&a,manoMesa*b){
	if(!a->esvacio(a)){
		if(b->Nombre==a->nombre){
			switch(b->Pinta){
				case 1:
				case 2:
					a->puntos=a->puntos+30;
					break;
				default:
					a->puntos=a->puntos+20;
					break;
			};
		}else{
			ganadorPuntos(a->Sig,b);
		}
	}
}

     /*	void lanzarcarta(manoMesa*&a,jugadorCO*&b){
		if(!b->esvacio(b)){
			if(!a->esvacio(a)) { //aca
				if(b->buscar(a->Pinta,b->Abajo)){
					b->lanzamayor();//TODO
				}else{
					lanzarmenor();//TODO
				}
			}else{
				lanzarprimero();  //TODO
			}
		}
	}  */
//----------------------------------------------------------------------------------------
void lanzar(jugadorCO*&a,manoMesa *&mesa,manoMesa *&k){   //
	if(!a->esvacio(a)){
		manoMesa *aux2;//en cada vuelta se crea el auxiliar cartas
		aux2=new manoMesa;   //este aux dos copia  la carta a lanzar de la mano del jugaor
		aux2->Nombre=a->nombre;
		if(mesa->esvacio(mesa)){ //si i la mesa es vaciia llama al metodo lanzar para que este seleccione s la carta mas adecuada de la mano
			mesa->lanzaralgo(a->Abajo,aux2);
			mesa->desbuscar(a->Abajo,aux2);//como los metodos lanzar no desapilan en este se pasa la copia de la carta y desapila
			mesa->apilar(mesa,aux2); //he de explicar esto?
			k=k->tomarmayor(k,aux2); //compara la carta lanzada con la mejor carta en la mesa hasta ahora
		}else{
			mesa->bucar2(a->Abajo,k->ultimo(mesa),aux2);  //toma la primera carta de la mesa y las compara con lo que posee el jugador en su mano de modo que pueda lanazar la carta correcta
			mesa->apilar(mesa,aux2);    //leer arriba
			mesa->desbuscar(a->Abajo,aux2); //  otra vez
			k=k->tomarmayor(k,aux2);       //uff
		}       //k es una auxiliar que posee la mejor carta lanzada en la mesa para poder asignar los puntos y guarrdar el nombre de quien gano la mesa
		lanzar(a->Sig,mesa,k); //llamada recursiva pasa al siguinete jugador
	}
}
//----------------------------------------------------------------------------------------
	void jugar2(jugadorCO *&a,manoMesa *&b,manoMesa *&c){   *a;*b;
		lanzar(a,b,c);    //metodo lanzar las cartas del jugador a la mesa
	}
	void jugar(AnsiString &ganador,jugadorCO *&a,manoMesa*&b,manoMesa *&c){
		c=NULL;     //ganador se le asigna nulo para que en la siguiente corrida no de errores
		if(!a->esvacio(a)){
			if(a->nombre==ganador){
				jugar2(a,b,c);//si esta parado en el jugador ganador, comienza el juego
			}else{			//sino, busca en la lista hasta conseguirlo
				jugadorCO *aux;
				aux=new jugadorCO;
				aux->nombre=a->nombre;
				aux->puntos=a->puntos;
				aux->Abajo=a->Abajo;
				a->desencolar(a);
				a->encolar(a,aux);
				jugar(ganador,a,b,c);   //busca su recursiva
			}
		}
	}
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------


int contarnodosMesa(manoMesa *a,int b){       //cuenta los nodos de la mesa
	if(a!=NULL){
		contarnodosMesa(a->Ant,b+1);	//recursiva para seguir contando
	}else{
		return b+1;
	}
}
int contarnodos(carta *a,int b){   //cuenta la cantidad de cartas
	if(a!=NULL){
		contarnodos(a->sig,b+1);
	}else{
		return b+1;
	}
}
void recurs(carta *a,int i,TStringGrid* b){      //recurdiva para mostrar los nodos en un stringgrid
	if(!a->esvacio(a)){
		switch(a->valor){
			case 14:
				b->Cells[1][i]="As";
				break;
			case 13:
				b->Cells[1][i]="K";
				break;
			case 12:
				b->Cells[1][i]="Q";
				break;
			case 11:
				b->Cells[1][i]="J";
				break;
			default:
				b->Cells[1][i]=a->valor;
		}
		switch (a->pinta){
			case 1:
				b->Cells[0][i]="Pica";
				break;
			case 2:
				b->Cells[0][i]="Corazon";
				break;
			case 3:
				b->Cells[0][i]="Diamante";
				break;
			case 4:
				b->Cells[0][i]="Trebol";
				break;
		};
		recurs(a->sig,i+1,b);
	}
}
//----------------------------------------------------------------------------------------
void recursMesa(manoMesa *a,int i,TStringGrid* b){
	if(!a->esvacio(a)){
		recursMesa(a->Ant,i-1,b);
		b->Cells[2][i]=a->Nombre;
		switch(a->Valor){
			case 14:
				b->Cells[1][i]="As";
				break;
			case 13:
				b->Cells[1][i]="K";
				break;
			case 12:
				b->Cells[1][i]="Q";
				break;
			case 11:
				b->Cells[1][i]="J";
				break;
			default:
				b->Cells[1][i]=a->Valor;
		}
		switch (a->Pinta){
			case 1:
				b->Cells[0][i]="Pica";
				break;
			case 2:
				b->Cells[0][i]="Corazon";
				break;
			case 3:
				b->Cells[0][i]="Diamante";
				break;
			case 4:
				b->Cells[0][i]="Trebol";
				break;
		};
	}
}

void updatlistMesa(manoMesa *a, TStringGrid* b){
	b->RowCount=contarnodosMesa(a,0);
	b->Cells[0][0]="Pinta";
	b->Cells[1][0]="Valor";
	b->Cells[2][0]="jugador";
	recursMesa(a,contarnodosMesa(a,0)-1,b);
}
void updatlist(carta *a, TStringGrid* b){//metodo que actualiza las gridlist
	b->RowCount=contarnodos(a,0);
	b->Cells[0][0]="Pinta";
	b->Cells[1][0]="Valor";
	recurs(a,1,b);//recursiva que actualiza todas las cartas
}
int contarnodosPI(cartasPI *a,int b){//metodo que cuenta la cantidad de cartas en la mano
	if(a!=NULL){
		contarnodosPI(a->Abajo,b+1);
	}else{
		return b+1;
	}
}
void recursPI(cartasPI *a,int i,TStringGrid* b){//recursiva que recorre la pila de cartas de las manos de los jugadores
	if(!a->esvacioPi(a)){
		switch(a->valor){    //si son mas de 10 muestra el valor en texto
			case 14:
				b->Cells[1][i]="As";
				break;
			case 13:
				b->Cells[1][i]="K";
				break;
			case 12:
				b->Cells[1][i]="Q";
				break;
			case 11:
				b->Cells[1][i]="J";
				break;
			default:
				b->Cells[1][i]=a->valor;
		}
		switch (a->pinta){  //muestra la pinta
			case 1:
				b->Cells[0][i]="Pica";
				break;
			case 2:
				b->Cells[0][i]="Corazon";
				break;
			case 3:
				b->Cells[0][i]="Diamante";
				break;
			case 4:
				b->Cells[0][i]="Trebol";
				break;
		};
		recursPI(a->Abajo,i+1,b);   //llama la recursion
	}
}
void updatlistPI(jugadorCO *a,AnsiString c, TStringGrid* b){ //actualiza las cartas de pila
	if(!a->esvacio(a)){
		if(a->nombre==c){
			b->RowCount=contarnodosPI(a->Abajo,0);
			b->Cells[0][0]="Pinta";
			b->Cells[1][0]="Valor";
			recursPI(a->Abajo,1,b);
		}else{
			updatlistPI(a->Sig,c,b); //recursion
		}
	}
}
//----------------------------------------------------------------------------------------
void NuevosJugadores(jugadorCO *&a,cartasDI *&b){  //crea la lista de jugadores
	a=new jugadorCO;
	a->nombre="Este";
	a->encolar(a,a->nvo("Norte"));
	a->encolar(a,a->nvo("Oeste"));
	a->encolar(a,a->nvo("Sur"));
	a->Repartir(a,b);    //reparte las cartas a los jugadores
}

//---------------------------------------------------------------------
__fastcall TMDIChild::TMDIChild(TComponent *Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------
void __fastcall TMDIChild::FormClose(TObject *Sender, TCloseAction &Action)
{
	Action = caFree;
}
//---------------------------------------------------------------------
void mazoADipolo(cartasDI *&a){                        *a;
	carta *b=new carta;
	b->Nvomazo(b);
	a->copiarMazo(a,b);
}
void __fastcall TMDIChild::InicioClick(TObject *Sender)
{
	mazoADipolo(cartasDipolo);
	updatlist(cartasDipolo->P,GridNorte);
		if(!cartasDipolo->Esvacio(cartasDipolo)){
		cartasDI *aux;
		aux=new cartasDI;
		aux=NULL;
		cartasDipolo->baraja(cartasDipolo);
		//objDI->barajar(objDI,aux);
		updatlist(cartasDipolo->P,GridNorte);
	}
	NuevosJugadores(jugadores,cartasDipolo);
	updatlistPI(jugadores,"Norte",GridNorte); /*aca va a chequear que entre en la cola y
	pregunte por el jugador norte para mostrar sus cartas*/
	updatlistPI(jugadores,"Sur",GridSur);/*aqui con el sur y es lo mismo para el este y
	oeste*/
	updatlistPI(jugadores,"Este",GridEste);
	updatlistPI(jugadores,"Oeste",GridOeste);

}
//---------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

void __fastcall TMDIChild::Button2Click(TObject *Sender)
{
	NuevosJugadores(jugadores,cartasDipolo);
	updatlistPI(jugadores,"Norte",GridNorte); /*aca va a chequear que entre en la cola y
	pregunte por el jugador norte para mostrar sus cartas*/
	updatlistPI(jugadores,"Sur",GridSur);/*aqui con el sur y es lo mismo para el este y
	oeste*/
	updatlistPI(jugadores,"Este",GridEste);
	updatlistPI(jugadores,"Oeste",GridOeste);
}
//---------------------------------------------------------------------------
void __fastcall TMDIChild::Button1Click(TObject *Sender)
{
	if(!cartasDipolo->Esvacio(cartasDipolo)){
		cartasDI *aux;
		aux=new cartasDI;
		aux=NULL;
		cartasDipolo->baraja(cartasDipolo);
		//objDI->barajar(objDI,aux);
		updatlist(cartasDipolo->P,GridNorte);
	}
}
//---------------------------------------------------------------------------
int puntosJugador(jugadorCO *a,AnsiString b){
	if(!a->esvacio(a)){
		if(a->nombre==b){
			return a->puntos;
		}
		else{
			return puntosJugador(a->Sig,b);
		}
	}
}

void mostrarGanador(jugadorCO *a){
	if(puntosJugador(a,"Norte")+puntosJugador(a,"Sur")==puntosJugador(a,"Este")+puntosJugador(a,"Oeste")){
		Application->MessageBoxA("Los Jugadores Empataron el Juego! vamos por la revancha","EMPATE",0);
	}else{
		if(puntosJugador(a,"Norte")+puntosJugador(a,"Sur")>puntosJugador(a,"Este")+puntosJugador(a,"Oeste")){
			ShowMessage("Los ganadores del juego son Norte y Sur con: "+AnsiString(puntosJugador(a,"Norte")+puntosJugador(a,"Sur"))+" Puntos");
		}else{
			ShowMessage("Los ganadores del juego son Este y Oeste con: "+AnsiString(puntosJugador(a,"Este")+puntosJugador(a,"Oeste"))+" Puntos");
		}
	}
}

void __fastcall TMDIChild::BitBtn1Click(TObject *Sender)
{      //INicio del juego
	if(!jugadores->esvacio(jugadores)){
	if(!jugadores->Abajo->esvacioPi(jugadores->Abajo)){ //si la pila de la mano de los jugadores no esta vacia
		if(contarnodosPI(jugadores->Abajo,0)==14){//si tiene todas las cartas
			jugar("Norte",jugadores,mesa,e);//juega con norte como primero
		}else{
			jugar(e->Nombre,jugadores,mesa,e);//juega con el ganador de la partida anterior
		}
		ganadorPuntos(jugadores,e);//muestra los ganadores del juego de acuerdo a la canidad de puntos
		updatlistMesa(mesa,StringGrid1);//actualiza la lista de cartas de la mesa
		updatlistPI(jugadores,"Norte",GridNorte);//actualiza las cartas de norte
		updatlistPI(jugadores,"Sur",GridSur);//actualiza las cartas de sur
		updatlistPI(jugadores,"Este",GridEste);//actualiza las cartas de este
		updatlistPI(jugadores,"Oeste",GridOeste);//actualiza las cartas de oeste
		Label7->Caption="Ganador: "+e->Nombre;//muestra el jugador que gano la mano o baza
		mesa=NULL;//limpia la mesa
		if(contarnodosPI(jugadores->Abajo,0)==1){//si se terminaron las cartas entra
			mostrarGanador(jugadores);//muestra el equipo ganador
			jugadores=NULL;//limpia los jugadores
		}
	}
	}else{
		Application->MessageBoxA("Favor, presione el bot�n inicio para comenzar el juego","Faltan los Jugadores",0);
	}
}
//---------------------------------------------------------------------------


//----------------------------------------------------------------------------
#ifndef MainH
#define MainH
//----------------------------------------------------------------------------
#include "ChildWin.h"
#include <vcl\ComCtrls.hpp>
#include <vcl\ExtCtrls.hpp>
#include <vcl\Messages.hpp>
#include <vcl\Buttons.hpp>
#include <vcl\Dialogs.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Menus.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\Classes.hpp>
#include <vcl\SysUtils.hpp>
#include <vcl\Windows.hpp>
#include <vcl\System.hpp>
#include <ActnList.hpp>
#include <ImgList.hpp>
#include <StdActns.hpp>
#include <ToolWin.hpp>
#include <jpeg.hpp>
//----------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:
	TMainMenu *MainMenu1;
	TMenuItem *File1;
	TMenuItem *FileNewItem;
	TMenuItem *Help1;
	TMenuItem *FileExitItem;
	TMenuItem *HelpAboutItem;
	TOpenDialog *OpenDialog;
	TActionList *ActionList1;
	TAction *FileNew1;
	TAction *FileExit1;
	TAction *HelpAbout1;
	TToolBar *ToolBar2;
	TToolButton *ToolButton9;
	TImageList *ImageList1;
	TMenuItem *Manual1;
	TImage *Image1;
	void __fastcall FileNew1Execute(TObject *Sender);
	void __fastcall HelpAbout1Execute(TObject *Sender);
	void __fastcall FileExit1Execute(TObject *Sender);
	void __fastcall Manual1Click(TObject *Sender);
//	void __fastcall Manual1Click(TObject *Sender);
private:
	void __fastcall CreateMDIChild(const String Name);
public:
	virtual __fastcall TMainForm(TComponent *Owner);
};
//----------------------------------------------------------------------------
extern TMainForm *MainForm;
extern TMDIChild *__fastcall MDIChildCreate(void);
//----------------------------------------------------------------------------
#endif

#ifndef CARTAS
#define CARTAS

#include <cstdlib>
#include <string>
#include <ctime>

class carta{
public:
	int pinta;
	/*
	las pintas son
		P para picas       =4
		C para corazones   =3
		T para treboles    =2
		D para diamanantes =1
	*/
	int valor;
	/*
	los valores van de:
		A para el As  =14
		2 a 10
		J             =11
		Q             =12
		K             =13
	*/
	carta *ant;
	carta *sig;
//----------------------------------------------------------------------------------------
	bool esvacio(carta *a){
		if(a==NULL){
			return true;
		}else{
			return false;
		}
	}
//----------------------------------------------------------------------------------------
	carta(){
		ant=NULL;
		sig=NULL;
		pinta=0;
		valor=0;
	}
//----------------------------------------------------------------------------------------
	void Nvomazo(carta *&a){  		//llamada al metodo que instanciara las cartas
		a=NULL;
		Crearmazo(2,1,a);
	}
//--------------------------------crear mazo----------------------------------------------
	void Crearmazo(int j, int i, carta *&mazo){   //metodo que crea el mazo
		if(i<=4){
			if(j<15){
				mazo = new carta();
				mazo->pinta = i;
				mazo->valor=j;
				Crearmazo(j+1,i, mazo->sig);
			}else{
				if(i+1!=5){
					mazo = new carta();
				}
				Crearmazo(2,i+1,mazo);
			}
		}
	}
};
//-------------------------------------DIPOLO---------------------------------------------
class cartasDI{            //clase de cartas de dipolo
public:
	carta *U;
	carta *P;
//------------------------------------copiar mazo-----------------------------------------
	void copiarMazo(cartasDI *&a,carta *b){    //copia el mazo de cartas a cartas en un dipolo
		if(!b->esvacio(b)){
			Postinsertar(a,b);
			copiarMazo(a,b->sig);
		}
	}
//-------------------------------------metodo constructor---------------------------------
	cartasDI() {	//instanciador del objeto dipolo
		P=NULL;
		U=NULL;
	}
//--------------------------------------esvacio-----------------------------------------
	bool Esvacio(cartasDI *D){
		if(D==NULL){
			return true;
		}else{
			return false;
		}
	}
//-------------------------------------primera carta--------------------------------------
	carta* primerochar(cartasDI *a){    //muestra la primera carta
		if(!Esvacio(a)){
			carta *aux=new carta;
			aux->pinta=a->P->pinta;
			aux->valor=a->P->valor;
			return aux;
		}else{
			return NULL;
		}
	}
//--------------------------------------postinsertar--------------------------------------
	void Postinsertar(cartasDI *&D, carta *E){    //inserta las cartas al final de la lista
		if(Esvacio(D)||D->P==NULL&&D->U==NULL){
			D=new cartasDI;
			D->P=E;
			D->U=E;
		}else{
			E->ant=D->U;
			D->U->sig= E;
			D->U=E;
		}
	}
//------------------------------------preeliminar-----------------------------------------
	void Preliminar(cartasDI *&a){		//Preelmimina la primera carta en el dipolo
		if(!Esvacio(a)){
			if(a->P==a->U){
				a=NULL;
			}else{
				a->P=a->P->sig;
				a->P->ant=NULL;
			}
		}
	}
	void copiar(cartasDI *&a,cartasDI *b){   //copia las cartas de un mazo a otro
		if(!Esvacio(b)){
			Postinsertar(a,primerochar(b));
		}
	}
	void pasar (cartasDI *&a,cartasDI *b){  //pasa las cartas de un dipolo a otro
		if (!Esvacio(b)){
			copiar(a,b);
			Preliminar(b);
			pasar(a,b);
		}
	}
	void baraja22(cartasDI *&d,cartasDI *&aux1,cartasDI *&aux2,cartasDI *&aux3,cartasDI *&aux4){
		int lugar=0;           //baraja las cartas con 4 dipolos lanzandolos de manera aleatoria y reordenandolos
		if (!Esvacio(d)){
			srand( rand()+time(0));
			lugar=rand() % 4+1;
			switch( lugar ){
				case 1 :
					copiar(aux1,d);
					Preliminar(d);
					break;
				case 2 :
					copiar(aux2,d);
					Preliminar(d);
					break;
				case 3 :
					copiar(aux3,d);
					Preliminar(d);
					break;
				case 4 :
					copiar(aux4,d);
					Preliminar(d);
					break;
			}
				baraja22(d,aux1,aux2,aux3,aux4);
		}else{
			pasar(d,aux1);
			pasar(d,aux2);
			pasar(d,aux3);
			pasar(d,aux4);
		}
	}
	void baraja(cartasDI *&d){	//metodo que llama al barajar 22 que desorndena las cartas
		srand(rand()+time(0));
		int ran=rand()%5+5;//para que sea de 5 a 10 recursiones
		for(int i=0;i<ran;i++){
			baraja22(d,NULL,NULL,NULL,NULL);
		}
	}
};

#endif

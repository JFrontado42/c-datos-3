object Apuestas: TApuestas
  Left = 0
  Top = 0
  Width = 217
  Height = 185
  AutoSize = True
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 217
    Height = 153
    Caption = 'Tipo de Apuesta'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 40
      Width = 51
      Height = 13
      Caption = 'Sin Triunfo'
    end
    object Label2: TLabel
      Left = 8
      Top = 62
      Width = 50
      Height = 13
      Caption = 'Corazones'
    end
    object Label3: TLabel
      Left = 8
      Top = 84
      Width = 26
      Height = 13
      Caption = 'Picas'
    end
    object Label4: TLabel
      Left = 8
      Top = 106
      Width = 41
      Height = 13
      Caption = 'Treboles'
    end
    object Label5: TLabel
      Left = 8
      Top = 128
      Width = 50
      Height = 13
      Caption = 'Diamantes'
    end
    object Label6: TLabel
      Left = 88
      Top = 16
      Width = 114
      Height = 13
      Caption = '1    2    3    4    5    6    7'
    end
    object RadioButton1: TRadioButton
      Left = 83
      Top = 40
      Width = 17
      Height = 17
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object RadioButton2: TRadioButton
      Left = 83
      Top = 62
      Width = 17
      Height = 17
      TabOrder = 1
    end
    object RadioButton3: TRadioButton
      Left = 83
      Top = 84
      Width = 17
      Height = 17
      TabOrder = 2
    end
    object RadioButton4: TRadioButton
      Left = 83
      Top = 106
      Width = 17
      Height = 17
      TabOrder = 3
    end
    object RadioButton5: TRadioButton
      Left = 83
      Top = 128
      Width = 17
      Height = 17
      TabOrder = 4
    end
    object RadioButton10: TRadioButton
      Left = 101
      Top = 128
      Width = 17
      Height = 17
      TabOrder = 5
    end
    object RadioButton9: TRadioButton
      Left = 101
      Top = 106
      Width = 17
      Height = 17
      TabOrder = 6
    end
    object RadioButton8: TRadioButton
      Left = 101
      Top = 84
      Width = 17
      Height = 17
      TabOrder = 7
    end
    object RadioButton7: TRadioButton
      Left = 101
      Top = 62
      Width = 17
      Height = 17
      TabOrder = 8
    end
    object RadioButton6: TRadioButton
      Left = 101
      Top = 40
      Width = 17
      Height = 17
      TabOrder = 9
    end
    object RadioButton15: TRadioButton
      Left = 120
      Top = 128
      Width = 17
      Height = 17
      TabOrder = 10
    end
    object RadioButton14: TRadioButton
      Left = 120
      Top = 106
      Width = 17
      Height = 17
      TabOrder = 11
    end
    object RadioButton13: TRadioButton
      Left = 120
      Top = 84
      Width = 17
      Height = 17
      TabOrder = 12
    end
    object RadioButton12: TRadioButton
      Left = 120
      Top = 62
      Width = 17
      Height = 17
      TabOrder = 13
    end
    object RadioButton11: TRadioButton
      Left = 120
      Top = 40
      Width = 17
      Height = 17
      TabOrder = 14
    end
    object RadioButton16: TRadioButton
      Left = 156
      Top = 106
      Width = 16
      Height = 17
      TabOrder = 15
    end
    object RadioButton17: TRadioButton
      Left = 156
      Top = 84
      Width = 16
      Height = 17
      TabOrder = 16
    end
    object RadioButton18: TRadioButton
      Left = 156
      Top = 62
      Width = 16
      Height = 17
      TabOrder = 17
    end
    object RadioButton19: TRadioButton
      Left = 156
      Top = 40
      Width = 16
      Height = 17
      TabOrder = 18
    end
    object RadioButton20: TRadioButton
      Left = 138
      Top = 128
      Width = 16
      Height = 17
      TabOrder = 19
    end
    object RadioButton21: TRadioButton
      Left = 138
      Top = 106
      Width = 16
      Height = 17
      TabOrder = 20
    end
    object RadioButton22: TRadioButton
      Left = 138
      Top = 84
      Width = 16
      Height = 17
      TabOrder = 21
    end
    object RadioButton23: TRadioButton
      Left = 138
      Top = 62
      Width = 16
      Height = 17
      TabOrder = 22
    end
    object RadioButton24: TRadioButton
      Left = 175
      Top = 128
      Width = 16
      Height = 17
      TabOrder = 23
    end
    object RadioButton25: TRadioButton
      Left = 175
      Top = 106
      Width = 16
      Height = 17
      TabOrder = 24
    end
    object RadioButton26: TRadioButton
      Left = 175
      Top = 84
      Width = 16
      Height = 17
      TabOrder = 25
    end
    object RadioButton27: TRadioButton
      Left = 175
      Top = 62
      Width = 16
      Height = 17
      TabOrder = 26
    end
    object RadioButton28: TRadioButton
      Left = 175
      Top = 40
      Width = 16
      Height = 17
      TabOrder = 27
    end
    object RadioButton29: TRadioButton
      Left = 156
      Top = 128
      Width = 16
      Height = 17
      TabOrder = 28
    end
    object RadioButton30: TRadioButton
      Left = 138
      Top = 40
      Width = 16
      Height = 17
      TabOrder = 29
    end
    object RadioButton31: TRadioButton
      Left = 193
      Top = 40
      Width = 16
      Height = 17
      TabOrder = 30
    end
    object RadioButton35: TRadioButton
      Left = 193
      Top = 128
      Width = 16
      Height = 17
      TabOrder = 31
    end
    object RadioButton34: TRadioButton
      Left = 193
      Top = 106
      Width = 16
      Height = 17
      TabOrder = 32
    end
    object RadioButton33: TRadioButton
      Left = 193
      Top = 84
      Width = 16
      Height = 17
      TabOrder = 33
    end
    object RadioButton32: TRadioButton
      Left = 193
      Top = 62
      Width = 16
      Height = 17
      TabOrder = 34
    end
  end
  object Button1: TButton
    Left = 142
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Apostar'
    TabOrder = 1
  end
  object Button2: TButton
    Left = 0
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Pasar'
    TabOrder = 2
  end
end

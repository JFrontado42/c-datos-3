//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "cola.h"
#include "arbolAVL.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
AVL *A;
Cola *C;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::AlturaClick(TObject *Sender)
{
        Label1->Caption=A->altura(A);

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Edit1KeyPress(TObject *Sender, char &Key)
{
    if (!(Key >= '0' && Key <= '9')){
        Key = 0;
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Cargar_ColaClick(TObject *Sender)
{
C=NULL;
CargarC(C,Edit1->Text.c_str(),0);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Cargar_ABBClick(TObject *Sender)
{
A=NULL;
cargarHijosABB(C,A);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Edit2KeyPress(TObject *Sender, char &Key)
{
    if (!(Key >= '0' && Key <= '9')){
        Key = 0;
    }else{
        Edit3->Text="";
    }
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button1Click(TObject *Sender)
{
        C=NULL;
        CargarC(C,Edit2->Text.c_str(),0);
        A->EliminarH(A,C->PrimeroNum(C));
        C->Desencolar(C);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
C=NULL;
CargarC(C,Edit3->Text.c_str(),0);
if(A->Buscar(A,C->PrimeroNum(C))){
        Label3->Caption="True";
}else{
        Label3->Caption="False";
}
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Edit3KeyPress(TObject *Sender, char &Key)
{

    if (!(Key >= '0' && Key <= '9')){
        Key = 0;
    }else{
        Edit3->Text="";
    }
}
//---------------------------------------------------------------------------


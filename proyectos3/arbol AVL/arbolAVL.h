#ifndef arbolAVLH
#define arbolAVLH
#include "cola.h"
class AVL {
        public:
                int valor,FE;
                AVL *Der,*Izq;
        AVL (){
                valor=0;
                FE=0;
                Der=NULL;
                Izq=NULL;
        }
        bool EsvacioA (AVL *A){
                if(A==NULL){
                        return true;
                }else{
                        return false;
                }
        }
        int Valor(AVL *A){
                if (!EsvacioA(A)){
                        return A->valor;
                }else{
                        return 0;
                }
        }
        void CrearRaiz (AVL *&A, int I){
                AVL *Aux=new AVL();
                Aux->Izq=NULL;
                Aux->Der=NULL;
                Aux->valor=I;
                Aux->FE=0;
                A=Aux;
        }
        bool Buscar (AVL *A, int I){
                if(!EsvacioA(A)){
                        if(Valor(A)==I){
                                return true;
                        }else{
                                if(Valor(A)>I){
                                        return Buscar(A->Izq,I);
                                }else{
                                        return Buscar(A->Der,I);
                                }
                        }
                }else{
                        return false;
                }
        }
        void EliminarH (AVL *&A, int I){
                AVL *aux,*aux1,*otro;
                bool bo;
                if(!EsvacioA(A)){
                        if(Valor(A)>I){
                                EliminarH(A->Izq,I);
                        }else{
                                if(Valor(A)<I){
                                        EliminarH(A->Der,I);
                                }else{
                                        otro=A;
                                        if(EsvacioA(otro->Der)){
                                                A=otro->Izq;
                                        }else{
                                                if(EsvacioA(otro->Izq)){
                                                        A=otro->Der;
                                                }else{
                                                        aux=A->Izq;
                                                        bo=false;
                                                        while(!EsvacioA(aux->Der)){
                                                                aux1=aux;
                                                                aux=aux->Der;
                                                                bo=true;
                                                        }
                                                        A->valor=aux->valor;
                                                        otro=aux;
                                                        if(bo){
                                                                aux1->Der=aux->Izq;
                                                        }else{
                                                                A->Izq=aux->Izq;
                                                        }
                                                }
                                        }
                                        delete(otro);
                                        otro=new AVL();
                                }
                        }
                }
        }
        int PreOrden(AVL *A){
                if(!EsvacioA(A)){
                        PreOrden(A->Izq);
                        //Raiz
                        PreOrden(A->Der);
                }
        }
        void InOrden(AVL *A){
                if(!EsvacioA(A)){
                //Raiz
                InOrden(A->Izq);
                InOrden(A->Der);
                }
        }
        void PosOrden(AVL *A){
                if(!EsvacioA(A)){
                        PosOrden(A->Izq);
                        PosOrden(A->Der);
                        //Raiz
                }
        }
        void IncertarAVL(AVL *&N, bool &B, int I){
                AVL *otro,*N1,*N2,*t;
                if(!EsvacioA(N)){
                        if(I<Valor(N)){
                                IncertarAVL(N->Izq,B,I);
                                if(B){
                                        switch(N->FE){
                                                case 1: {
                                                        N->FE=0;
                                                        B=false;
                                                }break;
                                                case 0: {
                                                        N->FE=-1;//ponerFe(N,-1);
                                                }break;
                                                case -1: {
                                                        N1=N->Izq;
                                                        if(N1->FE<0){//RII
                                                                N->Izq=N1->Der;
                                                                N1->Der=N;
                                                                N->FE=0;
                                                                N=N1;
                                                        }else{
                                                                N2=N1->Der;//RID
                                                                N->Izq=N2->Der;
                                                                N2->Der=N;
                                                                N1->Der=N2->Izq;
                                                                N2->Izq=N1;

                                                                if(N2->FE==-1){
                                                                        N->FE=1;
                                                                }else{
                                                                        N->FE=0;
                                                                }

                                                                if(N2->FE==1){
                                                                        N1->FE=-1;
                                                                }else{
                                                                        N1->FE=0;
                                                                }
                                                                N=N2;//termina RID
                                                        }
                                                        N->FE=0;
                                                        B=false;
                                                }break;
                                        }
                                }
                        }else{
                                if(I>Valor(N)){
                                        IncertarAVL(N->Der,B,I);
                                        if(B){
                                                switch(N->FE){
                                                        case -1: {
                                                                N->FE=0;
                                                                B=false;
                                                        }break;
                                                        case 0: {
                                                                N->FE=1;
                                                        }break;
                                                        case 1: {
                                                                N1=N->Der;
                                                                if(N1->FE>0){//RDD
                                                                        N->Der=N1->Izq;
                                                                        N1->Izq=N;
                                                                        N->FE=0;
                                                                        N=N1;//termina RDD
                                                                }else{
                                                                        N2=N1->Izq;//RDI
                                                                        N->Der=N2->Izq;
                                                                        N2->Izq=N;
                                                                        N1->Izq=N2->Der;
                                                                        N2->Der=N1;
                                                                        if(N2->FE==1){
                                                                                N->FE=-1;
                                                                        }else{
                                                                                N->FE=0;
                                                                        }
                                                                        if(N2->FE==-1){
                                                                                N1->FE=1;
                                                                        }else{
                                                                                N1->FE=0;
                                                                        }
                                                                        N=N2;//termina RDI
                                                                }
                                                                N->FE=0;
                                                                B=false;
                                                        }break;
                                                }
                                        }
                                }
                        }
                }else{
                        CrearRaiz(N,I);
                        B=true;
                }
        }
        int altura(AVL *A){
                if(EsvacioA(A)){
	                return 0;
                }else{
                        return 1 +(mayor(altura(A->Izq),altura(A->Der)));
                }
        }
private:
        int mayor(int a, int b){
                if(a<b){
                        return b;
                }else{
                        return a;
                }
        }

};
void cargarHijosABB(Cola *&C, AVL *&A){
        if(!C->EsvacioC(C)){
                A->IncertarAVL(A,false,C->PrimeroNum(C));
                C->Desencolar(C);
                cargarHijosABB(C,A);
        }
}
#endif

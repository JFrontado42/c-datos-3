//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TEdit *Edit1;
        TEdit *Edit2;
        TEdit *Edit3;
        TEdit *Edit4;
        TLabel *Label1;
        TButton *Button1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TButton *Button2;
        TLabel *Label7;
        TEdit *Edit5;
        TEdit *Edit6;
        TButton *Button3;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
        struct Tbase{
                int valor;
        };
        struct nodo{
                char etiqueta;
                int nodo;
                struct nodo *sig;
                struct arco *ady;
        };
        struct arco{
                int valor;
                struct nodo *origen;
                struct nodo *destino;
                struct arco *sig;
        };
class Grafo{
public:
        struct nodo *inicio;
        int nnodos;


        Grafo ();//contructor
        nodo *LocalizaLabel (nodo *n,const char e);//devuelve el nodo del vertice a buscar
        bool ExisteArco (nodo *o, nodo  *d,arco *a);//debuelve si verdadero si el camino entre dos vertices existen
        /*int GrafoVacio ();*/
        float EtiqArco (nodo *o, nodo *d,nodo *a,arco *camino);//devuelve el valor de un camino que este dirijido desde "o" hasta "d"
        Grafo *CrearRaiz(const char dato);//crea el primer vertice
        void InsertarNodo (Grafo *&G,const char dato, nodo *p);//inserta vertice al final de la lista
        bool InsertarArco (nodo *o, nodo *d, int valor);//incerta un camino(dirijido desde "o"(orijen) hasta "d"(destino))
        /*void BorrarArco (nodo *o, nodo *d);
        void DesconectarNodo (nodo a_eliminar);
        void CopiarGrafo (Grafo *gr,Grafo &g);  */
        void DestroyGrafo(nodo *n,arco *aux);//destruye por completo el grafo

        void CrearNodo(Grafo *&g,const char dato);//es el que controla la incercion de los nodo(vertice)
        bool CrearArco(char o, char d,int valor);//es el que controla la incercion de los Arco(caminos)
};
Grafo::Grafo(){
        inicio = new nodo;
        inicio->etiqueta =NULL;
        inicio->nodo = NULL;
        inicio->ady  = NULL;
        inicio->sig  = NULL;
        nnodos=NULL;
}
Grafo *Grafo::CrearRaiz(const char dato){
        Grafo *G=new Grafo();
        G->inicio->etiqueta=dato;
        G->inicio->nodo=1;
        G->nnodos=1;
        return G;
}
nodo *Grafo::LocalizaLabel(nodo *n,const char e){//devuelve el nodo del vertice a buscar
        if(n!=NULL){
                if (n->etiqueta == e){//si el vertice es igual devolvemos el nodo
                        return n;//devuel ve el nodo
                }else{
                        return LocalizaLabel(n->sig,e);//abansamos hacia los otros vertices
                }
        }else{
                return NULL;//
        }

}
bool Grafo::ExisteArco(nodo *o, nodo  *d, arco *a){//debuelve si verdadero si el camino entre dos vertices existen
        if(a!=NULL){
                if ((a->origen==o) && (a->destino==d)){
                        return true;
                }else{
                        return ExisteArco (o,d,a->sig);
                }
        }else{
                return false;
        }
}
void Grafo::InsertarNodo(Grafo *&G, const char dato, nodo *p){ //inserta vertice al final de la lista
        if(G==NULL){ //si es nulo creamos la raiz
                G=G->CrearRaiz(dato); //para crear el primer vertice
        }else{
                nodo *aux;
                aux = new nodo;
                if(p->sig != NULL){
                        InsertarNodo(G,dato,p->sig);
                }else{
                        aux->etiqueta = dato;
                        aux->nodo = (p->nodo)+1;
                        aux->ady = NULL;
                        aux->sig = NULL;
                        p->sig = aux;
                        (this->nnodos)++;
                }
        }
}
//incerta un camino
//el camino es dirijido desde "o" hasta "d"
//el nodo del camino es almacenado en la lista de caminos del orijen "o"
bool Grafo::InsertarArco (nodo *o, nodo *d, int valor){
        if(o!=NULL && d!=NULL){//para crear un camino es nesesario tener un orijen y un destino
        //para eso los dos deben existir
                arco *aux;//auxiliar
                aux = new arco;//extructura para la variable aux
                aux->origen = o; //orijen del camino
                aux->destino = d;//destino del camino
                aux->valor = valor;//valor costo del camino
                aux->sig= o->ady; //el nuevo camino es colocado de primero en la lista
                o->ady = aux; //le damos el nuevo camino a la vertice de orijen
                return true;//se creo el camino de forma exitosa
        }else{
                return false;//el camino no se creo
        }
}
float Grafo::EtiqArco(nodo *o, nodo *d, nodo *a,arco *camino){//devuelve el valor de un camino que este dirijido desde "o" hasta "d"
        if(o!=NULL && d!=NULL){//si uno de los vertise es nulo no es posible que exista un camino
                if(a!=NULL){//si es nulo es que se an recorrido todos los vertices
                        if(a->ady!=NULL){//si no es nulo es porque existe(n) camino(s)
                                if(camino==NULL){
                                        camino=a->ady;//pasamos los caminos de vertice a la variable auxiliar(camino) para poder recorrer los
                                }
                                if ((camino->origen == o) && (camino->destino == d)){//si son iguales devolvemos el valor del camino
                                        return (camino->valor);//valor o costo del camino retornado
                                }else{
                                        if(camino->sig!=NULL){//si es nolo es porque no hay mas caminos en el vertice
                                                return EtiqArco(o,d,a,camino->sig);//abansamos hacia el siguiente camino
                                        }else{
                                                return EtiqArco(o,d,a->sig,NULL);//al acabarse los caminos abansamos al siguiente vertice
                                        }
                                }
                        }else{
                                return EtiqArco(o,d,a->sig,NULL);//al no aber caminos avansamos al siguiente vertice
                        }
                }else{
                        return 0;//al no exitir vertice al cual comprobar los caminos devolvemos 0
                }
        }else{
                return 0;//
        }
}
void Grafo::DestroyGrafo(nodo *n,arco *aux){//destruye por completo el grafo
        if((this->inicio)->sig != NULL){
                n = (this->inicio)->sig;
                if(n->ady != NULL){
                        aux = n->ady;
                        n->ady = aux->sig;
                        delete aux;//eliminamos un camino
                        DestroyGrafo(n,aux);
                }else{
                        (this->inicio)->sig = n->sig;
                        delete n;//eliminamos un vertice
                        DestroyGrafo(n,aux);
                }
        }else{
                delete (this->inicio);//destruemos lo ultimo que queda
        }

}
void Grafo::CrearNodo(Grafo *&g,const char dato){//es el que controla la incercion de los nodo(vertice)
        Grafo *a; //variable auxiliar
        if(g==NULL){//si es nulo creamos el primer vertice
                a=CrearRaiz(dato);//devuelve un grafpo
                g=new Grafo();//le creamos la extructura
                g->inicio=a->inicio;//pasamos los valores de la variable auxiliar a la que se va a debolver
                g->nnodos=a->nnodos;//pasamos los valores de la variable auxiliar a la que se va a debolver
        }else{
                if(LocalizaLabel(g->inicio,dato)==NULL){//si es nulo es por que no esta creado
                        InsertarNodo(g,dato,g->inicio);//creamos un nodo al final de la lista
                }
        }
}
bool Grafo::CrearArco(char o, char d,int valor){//es el que controla la incercion de los Arco(caminos)
        return InsertarArco(LocalizaLabel(inicio,o),LocalizaLabel(inicio,d),valor); //devuel�ve true si el arco fue incertado
        //si devuelbe falso es porque uno de los vertises no existe en la lista
}
/*
void Grafo::BorrarArco(nodo *o, nodo *d){
        arco *a,*ant;
        int enc=0;
        if (o->ady==NULL){
                return;
        }else{
                if (o->ady->destino==d){
                        a = o->ady;
                        o->ady = a->sig;
                        delete a;
                }else {
                        ant = o->ady;
                        a = ant->sig;
                        while (!enc && (a!=0)){
                                if (a->destino==d){
                                        enc=1;
                                }else {
                                        a = a->sig;
                                        ant = ant->sig;
                                }
                        }
                        if (a==0){
                                return;
                        }else {
                                ant->sig = a->sig;
                                delete a;
                        }
                }
        }
}*/

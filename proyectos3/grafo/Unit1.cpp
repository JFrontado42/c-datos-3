//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
Grafo *G,*GG;

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
G->CrearNodo(G,'a');
G->CrearNodo(G,'b');
G->CrearNodo(G,'c');
G->CrearNodo(G,'d');

G->CrearArco('a','b',2);
G->CrearArco('a','c',3);
G->CrearArco('a','d',4);
G->CrearArco('c','a',15);
G->ExisteArco(G->inicio,G->inicio->sig,G->inicio->ady);
//G->LocalizaLabel(G->inicio,'b');
G->EtiqArco(G->LocalizaLabel(G->inicio,'c'),G->LocalizaLabel(G->inicio,'a'),G->inicio,NULL);
//G->DestroyGrafo(new nodo,new arco);
//G->BorrarArco(G->LocalizaLabel(G->inicio,'b'),G->LocalizaLabel(G->inicio,'d'));
GG=new Grafo();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
G->CrearNodo(G,*Edit4->Text.c_str());
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
if(G->CrearArco(*Edit1->Text.c_str(),*Edit2->Text.c_str(),Edit3->Text.ToInt())){
        Label1->Caption="Camino Creado";
}else{
        Label1->Caption="Error: uno de los vertices no existe";
}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
int aux;

aux=G->EtiqArco(G->LocalizaLabel(G->inicio,*Edit5->Text.c_str()),G->LocalizaLabel(G->inicio,*Edit6->Text.c_str()),G->inicio,NULL);
if(aux!=0){
        Label8->Caption="si existe un camino y su valor o costo es de\n"+AnsiString(aux);
}else{
        Label8->Caption="no existe camino";
}
}
//---------------------------------------------------------------------------


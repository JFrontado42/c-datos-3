object Form1: TForm1
  Left = 205
  Top = 41
  Width = 436
  Height = 541
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 160
    Top = 272
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object Label2: TLabel
    Left = 184
    Top = 48
    Width = 60
    Height = 13
    Caption = 'Crear Vertise'
  end
  object Label3: TLabel
    Left = 184
    Top = 152
    Width = 63
    Height = 13
    Caption = 'Crear Camino'
  end
  object Label4: TLabel
    Left = 64
    Top = 176
    Width = 78
    Height = 13
    Caption = 'Vertice de Orijen'
  end
  object Label5: TLabel
    Left = 256
    Top = 176
    Width = 87
    Height = 13
    Caption = 'Vertice de Destino'
  end
  object Label6: TLabel
    Left = 160
    Top = 224
    Width = 116
    Height = 13
    Caption = 'Valor o costo del camino'
  end
  object Label7: TLabel
    Left = 176
    Top = 368
    Width = 93
    Height = 13
    Caption = #191'Existe un Camino?'
  end
  object Label8: TLabel
    Left = 168
    Top = 440
    Width = 32
    Height = 13
    Caption = 'Label8'
  end
  object Label9: TLabel
    Left = 72
    Top = 392
    Width = 78
    Height = 13
    Caption = 'Vertice de Orijen'
  end
  object Label10: TLabel
    Left = 256
    Top = 392
    Width = 87
    Height = 13
    Caption = 'Vertice de Destino'
  end
  object Edit1: TEdit
    Left = 64
    Top = 192
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 256
    Top = 192
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object Edit3: TEdit
    Left = 160
    Top = 240
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object Edit4: TEdit
    Left = 64
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object Button1: TButton
    Left = 200
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Crear Vertice '
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 184
    Top = 296
    Width = 75
    Height = 25
    Caption = 'Crear Camino'
    TabOrder = 5
    OnClick = Button2Click
  end
  object Edit5: TEdit
    Left = 72
    Top = 408
    Width = 121
    Height = 21
    TabOrder = 6
  end
  object Edit6: TEdit
    Left = 256
    Top = 408
    Width = 121
    Height = 21
    TabOrder = 7
  end
  object Button3: TButton
    Left = 184
    Top = 472
    Width = 75
    Height = 25
    Caption = 'Comprobar'
    TabOrder = 8
    OnClick = Button3Click
  end
end
